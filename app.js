/**
 * Server.
 */

var express = require('express'),
    http = require('http'),
    path = require('path');

var path = require('path');
var csrf = require('csurf');
var multer = require('multer');
var bodyParser = require('body-parser');
var session = require('cookie-session');

var config = require('./config/app.js');
var logger = new(require('./utils/logger.js')).Logger();

var app;
var routeHandlers = ['statics', 'accounts', 'user', 'avatar', 'admin'];

var registerRoutes = function(routeFiles) {
    routeFiles.forEach(function(routeFile) {
        var handler = require(__dirname + '/routes/' + routeFile);
        handler.route(app);
    });
};

var configExpress = function() {
    logger.info('Configuring Express Server...');
    app = express();

    // Disable sending X-Powered-By header
    app.disable('x-powered-by');

    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');

    app.use(function(req, res, next) {
        res.set(config.headers);
        next();
    }); // Mess around with people inspecting headers :P
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: false,
        limit: '10mb'
    }));
    app.use(multer({
        dest: config.uploadsDir
    }));
    app.use(session({
        keys: config.session.keys
    }));
    app.use(express.static(path.join(__dirname, 'public')));

    app.use(csrf());
    // error handler
    app.use(function(err, req, res, next) {
        // Ignore CSRF failure for /avatar path
        if (req.path.indexOf('/avatar') == 0)
            return next();

        if (err.code !== 'EBADCSRFTOKEN')
            return next(err);

        // handle CSRF token errors here
        res.status(403);
        res.send('CSRF check failed.');
    });

    app.use(function(req, res, next) {
        // Make user csrf token available to templates
        res.locals.csrf = req.csrfToken();

        // Monkeypatch res.locals object
        res.locals.put = function(o) {
            for (var i = 1; i < arguments.length; ++i)
                o[arguments[i]] = arguments[++i];
            return o;
        };

        // Make user data available to templates
        if (req.session.uid) {
            res.locals.user = {
                id: req.session ? req.session.uid : null,
                email: req.session ? req.session.email : null
            };
        }

        // Make environment available to templates
        res.locals.environment = app.get('env');

        next();
    });

    registerRoutes(routeHandlers);

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handlers

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.json({
                message: err.message,
                error: err
            });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send('An internal error occurred while trying to perform this action. Please check back later.');
    });
};

var setupRoutes = function() {
    logger.info('Setting up routes...');
    registerRoutes(routeHandlers);
};

var listen = function() {
    logger.info('Setup complete... Starting app...');
    app.listen(config.port, function() {
        logger.info('Express server listening on port ' + config.port);
        logger.info('Launch sequence complete. All systems go. App started.', true);
    });
};

module.exports = {};

module.exports.start = function() {
    configExpress();
    //setupRoutes();
    listen();
};
