;
var nodemailer = require('nodemailer');

var smtpTransport = nodemailer.createTransport("SMTP", {
    use_authentication: false
});

module.exports = {};

module.exports.sendCode = function(email, code, type, callback) {
    var link = type == 'verification' ? 'verify' :
        type == 'reset' ? 'user/change-password' :
        'user/verify-email-change';

    link = 'http://www.avatarpicker.com/' + link + '?code=' + code;

    var subject, text, html;
    if (type == 'reset') {
        subject = 'Password reset requested';
        text = 'Hi,\nYou (or someone on your behalf) recently requested a password reset for your AvatarPicker account. In order to reset your password, please copy and paste the below link into your browser\'s address bar.\n' + link + '\n\nIf you did not request a password reset simply ignore this email. \n\nIf you are having any trouble with resetting your password, just reply to this email, and we\'ll happily help you out.\n\nThanks for choosing AvatarPicker, and have a great day :)\nAvatarPicker Support';
        html = 'Hi,<br>You (or someone on your behalf) recently requested a password reset for your AvatarPicker account. In order to reset your password, simply visit the link below.<br><a href="' + link + '">Change Password</a>.<br><br>If you cannot click the above link, please copy and paste the below link into your browser\'s address bar.<br>' + link + '<br><br>If you did not request a password reset simply ignore this email.<br>If you are having any trouble with resetting your password, just reply to this email, and we\'ll happily help you out.<br><br>Thanks for choosing AvatarPicker, and have a great day :)<br>AvatarPicker Support';
    } else if (type == 'verification') {
        subject = 'Please verify your email address';
        text = 'Hi,\nYou (or someone on your behalf) recently signed up for a AvatarPicker account. Your account has been provisioned, and requires one last action on your part to activate. To activate your account, please copy and paste the below link into your browser\'s address bar.\n' + link + '\n\nIf you did not sign up for a AvatarPicker account, or if you believe you received this email by mistake, please let us know by replying to this email.\n\nIf you are having any trouble with activating your account, just reply to this email, and we\'ll happily help you out.\n\nThanks for signing up for AvatarPicker, and have a great day :)\nAvatarPicker Support';
        html = 'Hi,<br>You (or someone on your behalf) recently signed up for a AvatarPicker account. Your account has been provisioned, and requires one last action on your part to activate. To activate your account simply visit the link below.<br><a href="' + link + '">Verify Email</a>.<br><br>If you cannot click the above link, please copy and paste the below link into your browser\'s address bar.<br>' + link + '<br><br>If you did not sign up for a AvatarPicker account, or if you believe you received this email by mistake, please let us know by replying to this email.<br>If you are having any trouble with activating your account, just reply to this email, and we\'ll happily help you out.<br><br>Thanks for signing up for AvatarPicker, and have a great day :)<br>AvatarPicker Support';
    } else {
        subject = 'Email change requested';
        text = 'Hi,\nYou (or someone on your behalf) recently requested an email change for your AvatarPicker account. In order to change the email associated with your AvatarPicker account, please copy and paste the below link into your browser\'s address bar.\n' + link + '\n\nIf you did not request an email change simply ignore this email. \n\nIf you are having any trouble with changing your email, just reply to this email, and we\'ll happily help you out.\n\nThanks for choosing AvatarPicker, and have a great day :)\nAvatarPicker Support';
        html = 'Hi,<br>You (or someone on your behalf) recently requested an email change for your AvatarPicker account. In order to change the email associated with your AvatarPicker account, simply visit the link below.<br><a href="' + link + '">Change Email</a>.<br><br>If you cannot click the above link, please copy and paste the below link into your browser\'s address bar.<br>' + link + '<br><br>If you did not request an email change simply ignore this email.<br>If you are having any trouble with changing your email, just reply to this email, and we\'ll happily help you out.<br><br>Thanks for choosing AvatarPicker, and have a great day :)<br>AvatarPicker Support';
    }

    var msg = {
        from: 'AvatarPicker Support <support@avatarpicker.com>',
        to: email,
        subject: subject,
        text: text,
        html: html
    };
    smtpTransport.sendMail(msg, function(err, result) {
        if (err)
            console.log('Failed to send mail: ' + err);
        callback(null, {
            code: err ? 500 : 200
        });
    });
};
