/*
 * Application Configuration
 */

var fs = require('fs');
var extend = require('../utils/extend');

// Dev config
var env = process.env.NODE_ENV || 'development';

// Common config settings
var appRoot = fs.realpathSync(__dirname + '/../');
var config = {
    avatarsDir: appRoot + '/avatars',
    uploadsDir: appRoot + '/uploads',
    logger: {
        file: appRoot + '/logs/run.log',
        level: 'info'
    },
    nginx: {
        basedir: appRoot + '/public',
        uploadsdir: appRoot,
        configFile: appRoot + '/config/nginx.conf',
        certsdir: appRoot + '/config',
        certFile: 'www.avatarpicker.com.crt',
        keyFile: 'www.avatarpicker.com.key'
    },
    minify: {
        js: {
            merge: [],
            split: []
        },
        css: {
            merge: [],
            split: []
        },
    },
    db: {
        host: 'localhost',
        name: 'avatardb'
    },
    plans: [null, {
        name: 'Starter',
        requests: {
            max: 5000
        }
    }, {
        name: 'Regular',
        requests: {
            max: 20000
        }
    }, {
        name: 'Premier',
        requests: {
            max: 100000
        }
    }]
};

if ('development' == env) {
    var env_config = {
        session: {
            keys: ['b1f1a2721e0ef78d0ef4581c328e527aeb6a36177cce4008f7e6cb5018beff2d', 'b15e927c2a7f054c1e0cfb9d99ef8a4b47aa1f3863245050f3187197397e8fb8', 'bd372aa874bc7011cac49a95a5f82cb732a0c95755a9988ccf95ae7b19862b5a']
        },
        nginx: {
            certFile: 'dev.crt',
            keyFile: 'dev.key'
        },
        db: {
            user: 'avatarapi',
            pass: 'avatarpass'
        },
        port: 7000
    };
} else if ('production' == env) {
    var env_config = {
        analytics: {
            site: 'UA-58523570-1',
            widget: 'UA-58523570-2'
        },
        nginx: {
            certFile: 'www.avatarpicker.com.crt',
            keyFile: 'www.avatarpicker.com.key'
        },
        minify: {
            js: {
                merge: ['jquery.1.10.2', 'jquery.scrollto.1.4.6', 'bootstrap', 'app'],
                split: ['gauge']
            },
            css: {
                merge: ['bootstrap', 'bootstrap-responsive', 'app'],
                split: []
            },
        },
        session: {
            keys: ['b1f1a2721e0ef78d0ef4581c328e527aeb6a36177cce4008f7e6cb5018beff2d', 'b15e927c2a7f054c1e0cfb9d99ef8a4b47aa1f3863245050f3187197397e8fb8', 'bd372aa874bc7011cac49a95a5f82cb732a0c95755a9988ccf95ae7b19862b5a']
        },
        db: {
            user: 'avatarapi',
            pass: 'qwaszxwesdxc'
        },
        port: 7000
    };
}

module.exports = extend(true, config, env_config);
