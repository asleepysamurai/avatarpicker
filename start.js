/**
 * Starter script. Always start app via this script
 */

var config = require('./config/app.js');
var logger = new(require('./utils/logger.js').Logger)({
    file: config.logger.file,
    logLevel: config.logger.level
});

logger.info('Launch sequence initiated...', true);

var setup = require('./utils/setup.js');
var app = require('./app.js');

setup(function(err) {
    logger.info('Setting up app for environment');

    if (err)
        return logger.error('Houston, we have a problem: ' + err);
    else
        return app.start();
});
