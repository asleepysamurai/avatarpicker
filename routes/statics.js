/*
 * All static pages
 */

var randomInt = function(max, min) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var landingPage = function(req, res) {
    res.render('index', {
        title: 'User friendly, social, in-page avatar changes for your web app.'
    });
};

exports.route = function(app) {
    // Home page
    app.get('/', landingPage);
    app.get('/index', landingPage);

    // Pricing page
    app.get('/pricing', function(req, res) {
        res.render('pricing', {
            title: 'Pricing',
        });
    });

    // TOS page
    app.get('/terms', function(req, res) {
        res.render('terms', {
            title: 'Terms of Service'
        });
    });

    // Privacy page
    app.get('/privacy', function(req, res) {
        res.render('privacy', {
            title: 'Privacy Policy'
        });
    });

    // Docs
    app.get('/docs', function(req, res) {
        res.render('docs', {
            title: 'Documentation - Getting Started'
        });
    });
    app.get('/docs/getting-started', function(req, res) {
        res.render('docs', {
            title: 'Documentation - Getting Started'
        });
    });
    app.get('/docs/integration', function(req, res) {
        res.render('docs/integration', {
            title: 'Documentation - Integrating AvatarPicker'
        });
    });
    app.get('/docs/customization', function(req, res) {
        res.render('docs/customization', {
            title: 'Documentation - Customizing AvatarPicker'
        });
    });
    app.get('/docs/security', function(req, res) {
        res.render('docs/security', {
            title: 'Documentation - Securing AvatarPicker'
        });
    });
};
