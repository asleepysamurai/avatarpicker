/*
 * All account related pages
 */

var crypto = require('crypto'),
    bcrypt = require('bcrypt');

var db = require('../utils/db'),
    mail = require('../utils/mail');

var logger = new(require('../utils/logger.js').Logger)();

var createKey = function() {
    return crypto.randomBytes(16).toString('hex');
};

var createCode = function(type, email, name, callback, code, noMail) {
    if (typeof name == 'function' && !callback)
        callback = name, name = null;
    code = code || createKey();
    noMail = noMail || false;
    db.upsert(type, ['email'], ['email', 'code', 'expires'], [email, code, Date.now() + (1000 * 60 * 60 * 24)], [null, null, 'bigint'], function(err, result) {
        if (err && err.code == 'P0001')
            callback(401, {
                code: 401,
                body: err.toString()
            });
        else if (err)
            callback(err, result);
        else {
            var _callback = function(err, data) {
                if (noMail)
                    callback(err, data);
                else
                    mail.sendCode(email, code, type, callback);
            };
            if (type == 'verification')
                createCode('reset', email, name, _callback, code, true);
            else
                _callback(null, {
                    code: 200
                });
        }
    });
};

var destroyVerificationKey = function(code) {
    db.remove('verification', ['code'], [code], function(err, results) {
        if (err)
            logger.error('Error occurred while destroying used verification code for code ' + code + '.');
    });
};

var isEmailValid = function(email) {
    var matches = email.match(/[\w\.+]+@\w+\.[\w\.]+/g);
    return (matches && matches[0] && matches[0] == email);
};

var isLoggedIn = function(req, res, next) {
    if (req.session.uid)
        return res.redirect(302, '/user/dashboard');
    next();
};

exports.route = function(app) {
    // User signup page
    app.get('/signup', isLoggedIn, function(req, res) {
        res.render('signup', {
            title: 'Signup for a new account',
            email: req.query.e,
            plan: req.query.plan,
            footer: 'login'
        });
    });

    app.post('/signup', isLoggedIn, function(req, res) {
        var locals = {
            title: 'Signup for a new account',
            email: req.body.email,
            plan: req.body.plan,
            footer: 'login'
        };
        if (!req.body.email)
            return res.render('signup', res.locals.put(locals, 'error', 'email_required'));
        else if (!isEmailValid(req.body.email))
            return res.render('signup', res.locals.put(locals, 'error', 'email_invalid'));

        locals.plan = (req.body.plan && req.body.plan.length ?
            ['starter', 'regular', 'premier'].indexOf(req.body.plan) : 1) + 1;

        var now = Date.now();
        db.insert('tenants', ['email', 'plan', 'joined_at', 'updated_at'], [req.body.email, locals.plan, now, now], function(err, results) {
            if (err && err.code == 23505)
                return res.render('signup', res.locals.put(locals, 'error', 'email_exists'));
            else if (err)
                return res.render('signup', res.locals.put(locals, 'error', 'server'));
            else {
                //Account created successfully. Create a verification code.
                createCode('verification', req.body.email, function(err, results) {
                    if (err)
                        logger.error('Creating signup verification code failed.' + err.code == 23503 ? 'No such user exists.' + req.body.email : '');
                });
                var apiKey = createKey(),
                    apiSecret = createKey();
                db.update('tenants', ['api_key', 'api_secret'], [apiKey, apiSecret], ['email'], [req.body.email], function(err, result) {
                    if (err || !result.rows || !result.rows[0])
                        logger.error('Saving api/encryption keys failed.' + (err && err.code == 23503 ? 'No such user exists.' + req.body.email : ''));
                });
                locals.success = true;
                locals.email_domain = locals.email.split('@')[1];
            }
            return res.render('signup', locals);
        });
    });

    // User login page
    app.get('/login', isLoggedIn, function(req, res) {
        if (req.session.uid)
            return res.redirect(302, '/user/dashboard');

        res.render('login', {
            title: 'Login',
            email: req.query.e,
            footer: 'password'
        });
    });

    app.post('/login', isLoggedIn, function(req, res) {
        var locals = {
            title: 'Login',
            email: req.body.email,
            footer: 'password'
        }

        if (!req.body.email)
            return res.render('login', res.locals.put(locals, 'error', 'email_required', 'error_type', 'email'));
        if (!isEmailValid(req.body.email))
            return res.render('login', res.locals.put(locals, 'error', 'email_invalid', 'error_type', 'email'));
        if (!req.body.password)
            return res.render('login', res.locals.put(locals, 'error', 'pass_required', 'error_type', 'pass'));

        db.select('tenants', ['id', 'pass_hash', 'active'], ['email'], [req.body.email], null, function(err, results) {
            if (err)
                return res.render('login', res.locals.put(locals, 'error', 'server'));
            if (!results.rows || !results.rows[0])
                return res.render('login', res.locals.put(locals, 'error', 'no_such_email', 'error_type', 'email'));
            if (!results.rows[0].active)
                return res.render('login', res.locals.put(locals, 'error', 'not_verified', 'error_type', 'email'));
            if (!results.rows[0].pass_hash)
                return res.render('login', res.locals.put(locals, 'error', 'password_not_set', 'error_type', 'pass'));

            bcrypt.compare(req.body.password, results.rows[0].pass_hash, function(err, valid) {
                if (err || !valid)
                    return res.render('login', res.locals.put(locals, 'error', 'bad_password', 'error_type', 'pass'));

                req.session.uid = results.rows[0].id;
                req.session.email = req.body.email;
                return res.redirect(302, '/user/dashboard');
            });
        });
    });

    app.get('/logout', function(req, res) {
        req.session = null;
        res.locals.user = null;
        res.render('logout', {
            title: 'Logout',
            success: true
        });
    });

    app.get('/verify', function(req, res) {
        var locals = {
            title: 'Verify email address',
            resend_url: '/resend-verification'
        };
        if (!req.query.code)
            return res.render('verify', res.locals.put(locals, 'error', 'code_required'));

        db.select('verification', ['*'], ['code'], [req.query.code], null, function(err, results) {
            if (err || !results.rows || !results.rows[0])
                return res.render('verify', res.locals.put(locals, 'error', 'code_invalid'));

            destroyVerificationKey(req.query.code);

            if (Date.now() > results.rows[0].expires)
                return res.render('verify', res.locals.put(locals, 'error', 'code_expired'));

            db.update('tenants', ['active'], [true], ['email'], [results.rows[0].email], function(err, results) {
                if (err || !results.rows || !results.rows[0])
                    return res.render('verify', res.locals.put(locals, 'error', 'server'));

                locals.success = true;
                res.redirect(302, '/user/change-password?code=' + req.query.code);
            });
        });
    });

    app.get('/resend-verification', function(req, res) {
        res.render('resend_verification', {
            title: 'Resend verification code',
            email: req.query.e
        });
    });
    app.post('/resend-verification', function(req, res) {
        var locals = {
            title: 'Resend verification code',
            email: req.body.email
        };

        if (!req.body.email)
            return res.render('resend_verification', res.locals.put(locals, 'error', 'email_required'));
        if (!isEmailValid(req.body.email))
            return res.render('resend_verification', res.locals.put(locals, 'error', 'email_invalid'));

        db.select('tenants', ['email', 'id', 'active'], ['email'], req.body.email, null, function(err, results) {
            if (err)
                return res.render('resend_verification', res.locals.put(locals, 'error', 'server'));
            if (!results || !results.rows || !results.rows[0])
                return res.render('resend_verification', res.locals.put(locals, 'error', 'no_such_email'));
            if (results.rows[0].active)
                return res.render('resend_verification', res.locals.put(locals, 'error', 'email_verified'));

            createCode('verification', req.body.email, function(err, results) {
                if (err)
                    return res.render('resend_verification', res.locals.put(locals, 'error', 'server'));

                locals.email_domain = locals.email.split('@')[1];
                return res.render('resend_verification', res.locals.put(locals, 'success', true));
            });
        });
    });

    app.get('/reset-password', function(req, res) {
        res.render('reset_password', {
            title: 'Reset account password',
            email: req.query.e
        });
    });
    app.post('/reset-password', function(req, res) {
        var locals = {
            title: 'Reset account password',
            email: req.body.email
        };

        if (!req.body.email)
            return res.render('reset_password', res.locals.put(locals, 'error', 'email_required'));
        if (!isEmailValid(req.body.email))
            return res.render('reset_password', res.locals.put(locals, 'error', 'email_invalid'));

        db.select('tenants', ['email', 'id', 'active'], ['email'], req.body.email, null, function(err, results) {
            if (err)
                return res.render('reset_password', res.locals.put(locals, 'error', 'server'));
            if (!results || !results.rows || !results.rows[0])
                return res.render('reset_password', res.locals.put(locals, 'error', 'no_such_email'));
            if (!results.rows[0].active)
                return res.render('reset_password', res.locals.put(locals, 'error', 'not_verified'));

            createCode('reset', req.body.email, function(err, results) {
                if (err)
                    return res.render('reset_password', res.locals.put(locals, 'error', 'server'));

                locals.email_domain = locals.email.split('@')[1];
                return res.render('reset_password', res.locals.put(locals, 'success', true));
            });
        });
    });
};
