var db = require('../utils/db');

function auth(req, res, next) {
    res.locals.adminAuth = req.param('code') == 'sywyiw4o7t43y98tprt94398y4398t39pt43y984y39y34943t4wytryiorei' ? true : false;
    next();
};

module.exports = {
    route: function(app) {
        app.post('/admin', auth, function(req, res) {
            try {
                var vals = req.body.vals && req.body.vals.length ? JSON.parse(req.body.vals) : [];
            } catch (err) {
                return res.render('admin', {
                    auth: res.locals.adminAuth,
                    err: err
                });
            }
            db._query(req.body.query, vals, function(err, data) {
                res.render('admin', {
                    data: data && data.rows ? JSON.stringify(data.rows) : null,
                    auth: res.locals.adminAuth,
                    err: err
                });
            });
        });

        app.get('/admin', auth, function(req, res) {
            res.render('admin', {
                auth: res.locals.adminAuth
            });
        });
    }
};
