/**
 * Handles the routing for avatars
 */

var fs = require('fs');
var gm = require('gm');
var glob = require('glob');
var https = require('https');
var crypto = require('crypto');
var mkdirp = require('mkdirp');
var cheerio = require('cheerio');

var config = require('../config/app.js');

var db = require('../utils/db');
var logger = new(require('../utils/logger.js').Logger)();

var twitterCacheTime = 3 * 24 * 60 * 60 * 1000;
var fileSequenceRegex = /[0-9,a-z,A-Z,\/]+/g;

function buildSelfURL(req, file) {
    return '//www.avatarpicker.com/avatars/' + file;
};

function getPathFromName(fileName) {
    hashSplitLength = 4;

    var hash = fileName.split('.');
    var ext = hash[1];
    hash = hash[0];

    var path = [];
    var pathMaxLength = hash.length / hashSplitLength;
    for (var i = 0; i < hash.length; i += hashSplitLength)
        path.push(hash.substr(i, hashSplitLength));

    var fileName = path.pop();

    if (fileName[0] == '-') {
        //Filename is just the sequence number. So pop off tha actual filename.
        fileName = path.pop() + fileName;
    }

    return {
        ext: ext,
        file: fileName + '.' + ext,
        fileWithoutExt: fileName,
        path: path.join('/')
    };
};

function stripSequenceNumber(fileName) {
    var matches = fileName.match(fileSequenceRegex);
    return {
        fileName: matches[0],
        sequenceNumber: matches[1],
        ext: matches[2],
        strippedFileName: matches[0] + '.' + matches[2]
    };
};

function insertSequenceNumber(fileName, sequenceNumber) {
    var matches = fileName.match(fileSequenceRegex);
    return matches[0] + '-' + sequenceNumber + '.' + matches[1];
};

function ensureDirectory(path, callback) {
    mkdirp(path, callback);
};

function getTenantInfo(req, res, next) {
    if (req.query.t) {
        res.locals.tenant = {
            id: req.query.t
        };
        return next();
    }

    db.select('tenants', ['id', 'api_secret'], ['api_key', 'active'], [req.param('apiKey'), true], null, function(err, results) {
        if (err) {
            res.locals.err = {
                status: 500,
                err: 'Failed to retrieve tenant info.'
            };
            return next();
        }

        if (!results.rows || !results.rows[0]) {
            res.locals.err = {
                status: 403,
                err: 'Invalid apiKey.'
            };
            return next();
        }

        res.locals.tenant = results.rows[0];
        next();
    });
};

function authenticate(req, res, next) {
    var tenant = res.locals.tenant;

    if (!(tenant && tenant.api_secret)) {
        res.locals.err = {
            status: 500,
            err: 'No tenant info to authenticate against.'
        }
        return next();
    }

    var hashedChallenge = crypto.createHash('md5').update(req.param('challenge') + tenant.api_secret).digest('hex');
    if (hashedChallenge != req.param('hash')) {
        res.locals.err = {
            status: 401,
            err: 'Challenge failed.'
        }
        return next();
    }

    res.locals.authenticate = {
        status: 200
    };
    next();
};

function redirect(req, res, serviceURL, isFile) {
    return res.redirect(302, isFile ? buildSelfURL(req, serviceURL) : serviceURL);
};

function gravatar(req, res) {
    var url = '//www.gravatar.com/avatar/' + encodeURIComponent(req.params.id) + '?d=identicon';
    if (req.query.s)
        url += '&s=' + encodeURIComponent(req.query.s);
    redirect(req, res, url);
};

function twitter(req, res) {
    function scrape() {
        https.get('https://twitter.com/' + encodeURIComponent(req.params.id), function(twitterResponse) {
            if (twitterResponse.statusCode != 200)
                return res.status(500).end();

            var responseBody = '';
            twitterResponse.on('data', function(data) {
                responseBody += data;
            });
            twitterResponse.on('end', function() {
                var profileAvatar = cheerio.load(responseBody)('img.ProfileAvatar-image').attr('src');
                if (!profileAvatar)
                    return res.status(500).end();

                redirect(req, res, profileAvatar);

                var validity = parseInt(Date.now() / 1000) + (twitterCacheTime / 1000); // Convert to seconds (millisecond precision not required and wastes db) and add 5 mins
                db.upsert('twitter_cache', ['twitter_id'], ['twitter_id', 'url', 'valid_till'], [req.params.id, profileAvatar, validity], ['varchar', 'varchar', 'integer'], function(err, data) {});

                //Remove from cache after 5 mins
                setTimeout(function() {
                    db.remove('twitter_cache', ['twitter_id'], [req.params.id], function(err, data) {});
                }, twitterCacheTime);
            });
        }).on('error', function(err) {
            return res.status(500).end();
        });
    };

    if (req.query.cb && res.locals.authenticate && res.locals.authenticate.status == 200) {
        scrape();
    } else {
        db.select('twitter_cache', ['url', 'valid_till'], ['twitter_id'], [req.params.id], null, function(err, results) {
            if (err)
                return res.status(500).end();

            if (results.rows && results.rows[0] && results.rows[0].valid_till > parseInt(Date.now() / 1000)) {
                //Image in cache is valid
                return redirect(req, res, results.rows[0].url);
            }

            scrape();
        });
    }
};

function facebook(req, res) {
    var url = 'https://graph.facebook.com/' + encodeURIComponent(req.params.id) + '/picture';
    if (req.query.s)
        url += '?width=' + encodeURIComponent(req.query.s) + '&height=' + encodeURIComponent(req.query.s);
    redirect(req, res, url);
};

function avatar(req, res) {
    db.select('avatars', ['service', 'service_username'], ['user_hash', 'tenant_id', 'deleted'], [req.params.user, req.params.tenant, false], null, function(err, results) {
        if (err) {
            logger.error(err.message);
            return res.status(500).end();
        }
        if (!(results.rows && results.rows[0] && results.rows[0].service)) {
            if (req.query.d)
                return res.redirect(req.query.d);
            return res.status(404).end();
        }

        var url = '//www.avatarpicker.com/avatar/' + results.rows[0].service + '/' + results.rows[0].service_username + '?t=' + req.params.tenant;
        if (req.query.s)
            url += '&s=' + req.query.s;
        if (req.query.d)
            url += '&d=' + req.query.d;

        res.header('Cache-Control', 'no-store');
        res.redirect(302, url);
    });
};

function upload(req, res) {
    req.session.fileName = req.files.avatarFile.name;

    if (!req.files.avatarFile.name) {
        return res.json({
            status: 500,
            err: 'File upload failed.'
        });
    }

    res.json({
        status: 200,
        data: '/uploads/' + req.files.avatarFile.name
    });

    var msFor30mins = 30 * 60 * 1000;
    var fileName = config.uploadsDir + '/' + req.session.fileName;
    setTimeout(function() {
        fs.unlink(fileName, function(err) {
            if (err)
                logger.error('Failed to delete file: ' + fileName);
        });
    }, msFor30mins);
};

function edit(req, res) {
    if (res.locals.err)
        return res.json(res.locals.err);

    // Authentication succcessful - remove self hosted avatar
    var transforms = {
        width: parseInt(req.body.boundWidth) || null,
        height: parseInt(req.body.boundHeight) || null,
        y: parseInt(req.body.boundTop) || 0,
        x: parseInt(req.body.boundLeft) || 0,
        rotate: parseInt(req.body.rotate) || 0,
        flipV: req.body.flipV == -1 ? true : false,
        flipH: req.body.flipH == -1 ? true : false,
    };
    transforms.size = Math.min(transforms.width, transforms.height);

    var fileName = config.uploadsDir + '/' + req.session.fileName;
    var image = gm(fileName);

    image.identify(function(err, data) {
        if (err) {
            return res.json({
                status: 500,
                err: 'Failed to retrieve image info.'
            });
        }

        if (data.format == 'GIF')
            image = gm(fileName + '[0]');

        var size = data.size;
        if (transforms.flipH)
            image = image.flop();
        if (transforms.flipV)
            image = image.flip();
        if (transforms.rotate) {
            image = image.rotate('green', req.body.rotate);
        }

        if (transforms.size)
            image = image.crop(transforms.size, transforms.size, transforms.x, transforms.y);
        else {
            transforms.size = transforms.size || Math.min(size.width, size.height);
            image = image.crop(transforms.size, transforms.size, 0, 0);
        }

        var pathPrefix = config.avatarsDir + '/';
        var finalFileLocation = getPathFromName(req.session.fileName);
        var relativeFilePath = finalFileLocation.path + '/' + finalFileLocation.file;
        var finalDirectory = pathPrefix + finalFileLocation.path;

        ensureDirectory(finalDirectory, function(err) {
            if (err) {
                return res.json({
                    status: 500,
                    err: 'Failed to save edited image.'
                });
            }

            glob(finalDirectory + '/' + finalFileLocation.fileWithoutExt + '*', {
                nocase: true,
                nonull: false
            }, function(err, files) {
                if (err) {
                    return res.json({
                        status: 500,
                        err: 'Failed to save edited image.'
                    });
                }

                var sequenceNumber = (files.length || 0) + 1;
                relativeFilePath = insertSequenceNumber(relativeFilePath, sequenceNumber);

                req.session.fileName = relativeFilePath.replace(/\//g, '');

                image.noProfile().write(pathPrefix + relativeFilePath, function(err, data) {
                    if (err) {
                        return res.json({
                            status: 500,
                            err: 'Failed to edit image.'
                        });
                    }

                    res.json({
                        status: 200,
                        data: '/avatars/' + relativeFilePath
                    });
                });
            });
        });
    });
};

function self(req, res) {
    if (!res.locals.tenant && res.locals.err)
        return res.status(res.locals.err.status).end(res.locals.err.err);

    if (req.query.sopts && req.query.sopts == 1) {
        var redirectURL = '//www.avatarpicker.com/avatar/' + res.locals.tenant.id + '/' + req.params.id;
        if (req.query.s)
            redirectURL += '?s=' + req.query.s;
        return res.redirect(redirectURL);
    }

    // Authentication succcessful - retrieve self hosted avatar
    db.select('avatars_self', ['file_name'], ['user_hash', 'tenant_id', 'deleted'], [req.params.id, res.locals.tenant.id, false], null, function(err, results) {
        if (err)
            return res.status(500).end();

        if (!(results.rows && results.rows[0] && results.rows[0].file_name)) {
            if (req.query.d)
                return res.redirect(req.query.d);
            return res.status(404).end();
        }

        var fileDetails = stripSequenceNumber(results.rows[0].file_name);

        var finalFileLocation = getPathFromName(fileDetails.strippedFileName);
        finalFileLocation = finalFileLocation.path + '/' + finalFileLocation.file;
        finalFileLocation = insertSequenceNumber(finalFileLocation, fileDetails.sequenceNumber);

        redirect(req, res, finalFileLocation, true, res.locals.tenant);
    });
};

function remove(req, res) {
    if (!!!req.body.remove)
        return res.send();

    db.select('tenants', ['id', 'api_secret'], ['api_key', 'active'], [req.body.apiKey, true], null, function(err, results) {
        if (err) {
            return res.json({
                status: 500,
                err: 'Failed to retrieve tenant info.'
            });
        }

        if (!results.rows || !results.rows[0]) {
            return res.json({
                status: 403,
                err: 'Invalid apiKey.'
            });
        }

        var tenant = results.rows[0];
        var hashedChallenge = crypto.createHash('md5').update(req.body.challenge + tenant.api_secret).digest('hex');
        if (hashedChallenge != req.body.hash) {
            return res.json({
                status: 401,
                err: 'Challenge failed.'
            });
        }

        function deleteAvatar(tables, callback) {
            var results = [];
            var resultCount = 0;

            for (var i = 0; i < tables.length; ++i) {
                db.update(tables[i], ['deleted'], [true], ['user_hash', 'tenant_id'], [req.body.user, tenant.id], function(err, data) {
                    if (err)
                        return callback(err);

                    //Recursively remove dirs till you hit one that is not empty.
                    function removeDir(dir, callback) {
                        if (!dir.length)
                            return;

                        var absDir = config.avatarsDir + '/' + dir;
                        fs.rmdir(absDir, function(err, data) {
                            if (err) {
                                callback(err.code == 'ENOTEMPTY' ? null : err, dir);
                            } else {
                                var pathSplit = dir.split('/');
                                pathSplit.pop();
                                pathSplit = pathSplit.join('/');

                                removeDir(pathSplit, callback);
                            }
                        });
                    };

                    if (data && data.rows && data.rows[0] && data.rows[0].file_name) {
                        var fileLocation = getPathFromName(data.rows[0].file_name);
                        var absFilePath = config.avatarsDir + '/' + fileLocation.path + '/' + fileLocation.file;
                        fs.unlink(absFilePath, function(err) {
                            if (err)
                                return logger.error('Failed to delete file: ' + fileName + ' due to: ' + err);

                            removeDir(fileLocation.path, function(err, data) {
                                if (err)
                                    logger.error('Failed to delete directory: ' + data + ' due to: ' + err);
                            });
                        });
                    }

                    results.push(data);
                    ++resultCount;

                    if (resultCount == tables.length)
                        return callback(null, results);
                });
            }
        };

        // Authentication succcessful - remove self hosted avatar
        deleteAvatar(['avatars', 'avatars_self'], function(err, results) {
            if (err) {
                return res.json({
                    status: 500,
                    err: 'Failed to remove avatar'
                });
            }

            return res.json({
                status: 200,
                data: 'ok'
            });
        });
    });
};

function set(req, res) {
    if (!req.body.hasOwnProperty('service') || !req.body.hasOwnProperty('username')) {
        return res.json({
            status: 400,
            err: 'service/username missing.'
        });
    }

    if (['self', 'facebook', 'twitter', 'gravatar'].indexOf(req.body.service) == -1) {
        return res.json({
            status: 400,
            err: 'invalid service.'
        });
    }

    if (!res.locals.authenticate)
        return res.json(res.locals.err);

    var avatarUpsert = function(err, results) {
        db.upsert('avatars', ['user_hash', 'tenant_id'], ['user_hash', 'tenant_id', 'service', 'service_username', 'deleted'], [req.body.user, res.locals.tenant.id, req.body.service, req.body.username, false], ['varchar', 'integer', 'varchar', 'varchar', 'boolean'], function(err, results) {
            if (err) {
                return res.json({
                    status: 500,
                    err: 'Failed to update avatar.'
                });
            }
            return res.json({
                status: 200
            });
            //TODO: make a copy of image from serviceURL (if twitter) and put on our servers + cdns
        });
    };

    if (req.body.service == 'self') {
        db.upsert('avatars_self', ['user_hash', 'tenant_id'], ['user_hash', 'tenant_id', 'file_name', 'deleted'], [req.body.user, res.locals.tenant.id, req.session.fileName, false], ['varchar', 'integer', 'varchar', 'boolean'], function(err, results) {
            if (err) {
                return res.json({
                    status: 500,
                    err: 'Failed to update avatar self.'
                });
            }
            avatarUpsert();
            //TODO: make a copy of image from serviceURL (if twitter) and put on our servers + cdns
        });
    } else {
        avatarUpsert();
    }
};

function challenge(req, res) {
    if (!req.query.userhash) {
        return res.json({
            status: 400,
            err: 'Userhash is a mandatory parameter.'
        });
    }

    var tenant = res.locals.tenant;
    if (!(tenant && tenant.api_secret)) {
        return res.json({
            status: 500,
            err: 'No tenant info to authenticate against.'
        });
    }

    var challengeString = crypto.randomBytes(16).toString('hex');
    var hashedChallenge = crypto.createHash('md5').update(challengeString + tenant.api_secret).digest('hex');

    db.insert('user_challenges', ['tenant', 'user_hash', 'challenge'], [tenant.id, req.query.userhash, hashedChallenge], function(err, data) {
        if (err) {
            return res.json({
                status: 500,
                err: 'Failed to generate challenge string.'
            });
        }

        return res.json({
            status: 200,
            data: challengeString
        });
    });
};

module.exports = {
    route: function(app) {
        app.post('/avatar/upload', upload);
        app.post('/avatar/edit', getTenantInfo, authenticate, edit);
        app.post('/avatar/remove', remove);
        app.post('/avatar/set', getTenantInfo, authenticate, set);
        app.get('/avatar/gravatar/:id', getTenantInfo, gravatar);
        app.get('/avatar/twitter/:id', getTenantInfo, authenticate, twitter);
        app.get('/avatar/facebook/:id', getTenantInfo, facebook);
        app.get('/avatar/self/:id', getTenantInfo, self);
        app.get('/avatar/:tenant/:user', avatar);
    }
};
