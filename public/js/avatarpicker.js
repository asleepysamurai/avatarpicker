;
(function(global) {
    var opts;
    var widgetPrefix;
    var widgetContainer;
    var iframeNode;
    var finalCallback;
    var finalAvatarURL;

    function addEventListener(target, _event, listener) {
        var finalListener = listener;

        if (_event == 'scroll')
            addWheelListener(target, finalListener);
        else if (target.addEventListener)
            target.addEventListener(_event, finalListener);
        else if (target.attachEvent)
            target.attachEvent(_event, finalListener);

        if (_event == 'click' || _event == 'mousedown')
            addEventListener(target, 'touchstart', finalListener);
        else if (_event == 'mousemove')
            addEventListener(target, 'touchmove', finalListener);
        else if (_event == 'mouseup')
            addEventListener(target, 'touchend', finalListener);
    };

    /**
     * jQuery extend method - standalone version
     * From [https://github.com/dansdom/extend]
     */
    function merge() {
        var options, name, src, copy, copyIsArray, clone,
            target = arguments[0] || {},
            i = 1,
            length = arguments.length,
            deep = false,
            // helper which replicates the jquery internal functions
            objectHelper = {
                hasOwn: Object.prototype.hasOwnProperty,
                class2type: {},
                type: function(obj) {
                    return obj == null ?
                        String(obj) :
                        objectHelper.class2type[Object.prototype.toString.call(obj)] || "object";
                },
                isPlainObject: function(obj) {
                    if (!obj || objectHelper.type(obj) !== "object" || obj.nodeType || objectHelper.isWindow(obj)) {
                        return false;
                    }

                    try {
                        if (obj.constructor &&
                            !objectHelper.hasOwn.call(obj, "constructor") &&
                            !objectHelper.hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                            return false;
                        }
                    } catch (e) {
                        return false;
                    }

                    var key;
                    for (key in obj) {}

                    return key === undefined || objectHelper.hasOwn.call(obj, key);
                },
                isArray: Array.isArray || function(obj) {
                    return objectHelper.type(obj) === "array";
                },
                isFunction: function(obj) {
                    return objectHelper.type(obj) === "function";
                },
                isWindow: function(obj) {
                    return obj != null && obj == obj.window;
                }
            }; // end of objectHelper

        // Handle a deep copy situation
        if (typeof target === "boolean") {
            deep = target;
            target = arguments[1] || {};
            // skip the boolean and the target
            i = 2;
        }

        // Handle case when target is a string or something (possible in deep copy)
        if (typeof target !== "object" && !objectHelper.isFunction(target)) {
            target = {};
        }

        // If no second argument is used then this can extend an object that is using this method
        if (length === i) {
            target = this;
            --i;
        }

        for (; i < length; i++) {
            // Only deal with non-null/undefined values
            if ((options = arguments[i]) != null) {
                // Extend the base object
                for (name in options) {
                    src = target[name];
                    copy = options[name];

                    // Prevent never-ending loop
                    if (target === copy) {
                        continue;
                    }

                    // Recurse if we're merging plain objects or arrays
                    if (deep && copy && (objectHelper.isPlainObject(copy) || (copyIsArray = objectHelper.isArray(copy)))) {
                        if (copyIsArray) {
                            copyIsArray = false;
                            clone = src && objectHelper.isArray(src) ? src : [];

                        } else {
                            clone = src && objectHelper.isPlainObject(src) ? src : {};
                        }

                        // Never move original objects, clone them
                        target[name] = merge(deep, clone, copy);

                        // Don't bring in undefined values
                    } else if (copy !== undefined) {
                        target[name] = copy;
                    }
                }
            }
        }

        // Return the modified object
        return target;
    };

    function prefix(str) {
        str = str.split(' ');

        for (var i = 0; i < str.length; ++i)
            str[i] = str[i].length ? widgetPrefix + '-' + str[i] : str[i];

        return str.join(' ');
    };

    function buildElementFromArray(elementArray) {
        var elements = [];

        for (var j = 0; j < elementArray.length; ++j) {
            var props = elementArray[j];

            var element;

            if (typeof props == 'string') {
                element = document.createTextNode(props);
            } else {
                if (!props.element)
                    throw new Error('element property mandatory for root and non-text child nodes.');

                element = document.createElement(props.element);
                if (props.classNames)
                    element.className = prefix(props.classNames);

                if (props.element == 'button')
                    element.className += ' ' + prefix('button');

                if (props.events && props.events.length) {
                    for (var i = 0; i < props.events.length; ++i)
                        addEventListener(element, props.events[i].name, props.events[i].listener);
                }

                if (props.children && props.children.length) {
                    var childElements = buildElementFromArray(props.children);
                    for (var i = 0; i < childElements.length; ++i)
                        element.appendChild(childElements[i]);
                }

                var onCreate = props.onCreate;

                delete props.element;
                delete props.events;
                delete props.children;
                delete props.classNames;
                delete props.onCreate;

                for (var key in props) {
                    if (props.hasOwnProperty(key)) {
                        element.setAttribute(key, props[key]);
                        if (key == 'alt')
                            element.setAttribute('title', props[key]);
                    }
                }

                if (onCreate && onCreate.call)
                    onCreate(element);
            }

            elements.push(element);
        }

        return elements;
    };

    function buildStyles() {
        var widget = prefix('widget');

        return buildElementFromArray([{
            element: 'style',
            classNames: 'widgetStyles',
            children: ['div.' + widget + '{ \
                    position: fixed; \
                    left: 0; \
                    right: 0; \
                    top: 20px; \
                    background: #fff; \
                    margin: auto; \
                    border: 1px solid #e3e3e3; \
                    color: #555; \
                    -webkit-user-select: none; \
                    -moz-user-select: none; \
                    -user-select: none; \
                    max-width: 282px; \
                    height: 345px; \
                    z-index: 10001; \
                } \
                body { \
                    overflow: hidden; \
                } \
                div.' + widget + ', div.' + widget + ' div{ \
                    box-sizing: border-box; \
                } \
                div.' + widget + ' iframe{ \
                    max-width: 282px; \
                    height: 345px; \
                    z-index: 10001; \
                    border: 0px; \
                    overflow: hidden; \
                } \
                img.' + prefix('btn-close') + '{ \
                    position: absolute; \
                    top: 7px; \
                    right: 10px; \
                    cursor: pointer; \
                    display: inline-block; \
                } \
                div.' + prefix('widget-backdrop') + ' { \
                    position: fixed; \
                    top: 0; \
                    bottom: 0; \
                    left: 0; \
                    right: 0; \
                    background: #000; \
                    opacity: 0.7; \
                    z-index: 10000; \
                } \
                ']
        }])[0];
    };

    function removeWidget(ev) {
        preventEvent(ev);
        cleanup();
        finalCallback && finalCallback.call && finalCallback(finalAvatarURL);
    };

    function initWidgetContainer(opts) {
        var iSrc = 'https://www.avatarpicker.com/widget.html?';

        /*
        for (var key in opts) {
            if (opts.hasOwnProperty(key) && opts[key])
                iSrc += key + '=' + opts[key] + '&';
        }
        */

        var widgetContainer = buildElementFromArray([{
            element: 'div',
            classNames: 'widget shadow-90',
            children: [{
                element: 'iframe',
                src: iSrc,
                onCreate: function(iframe) {
                    iframeNode = iframe;
                },
                events: [{
                    name: 'load',
                    listener: function(ev) {
                        iframeNode.contentWindow.postMessage(JSON.stringify(opts), 'https://www.avatarpicker.com');
                    }
                }]
            }, {
                element: 'img',
                classNames: 'btn-close',
                src: window.location.protocol + '//www.avatarpicker.com/img/close.png',
                alt: 'Close',
                events: [{
                    name: 'click',
                    listener: removeWidget
                }]
            }]
        }])[0];

        return widgetContainer;
    };

    function initWidgetBackdrop() {
        var widgetBackdrop = buildElementFromArray([{
            element: 'div',
            classNames: 'widget-backdrop',
            events: [{
                name: 'click',
                listener: removeWidget
            }]
        }])[0];

        return widgetBackdrop;
    };

    function preventEvent(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    };

    function cleanup() {
        if (!widgetPrefix)
            return;

        var styleNode = document.querySelectorAll('style.' + prefix('widgetStyles'))[0];
        var widgetContainerNode = document.querySelectorAll('div.' + prefix('widget'))[0];
        var widgetBackdrop = document.querySelectorAll('div.' + prefix('widget-backdrop'))[0];

        if (styleNode)
            document.body.removeChild(styleNode);
        if (widgetContainerNode)
            document.body.removeChild(widgetContainerNode);
        if (widgetBackdrop)
            document.body.removeChild(widgetBackdrop);
    };

    function init(customOpts) {
        opts = customOpts;

        cleanup();
        errCount = 0;
        widgetPrefix = '_' + Math.random().toString().substr(2);

        if (!opts.userhash)
            throw new Error('No userhash provided. Userhash is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/userhash for details.');
        if (!opts.challenge)
            throw new Error('No challenge provided. Challenge is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/auth for details.');
        if (!opts.challengeHash)
            throw new Error('No challengeHash provided. Challenge hash is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/auth for details.');
        if (!opts.apiKey)
            throw new Error('No apiKey provided. API key is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/auth for details.');

        var widgetStyles = buildStyles();
        document.body.appendChild(widgetStyles);

        widgetContainer = initWidgetContainer(opts);
        var widgetBackdrop = initWidgetBackdrop();

        document.body.appendChild(widgetContainer);
        document.body.appendChild(widgetBackdrop)
    };

    //Testing only
    //supports = {fileReader: false, canvas:false};

    /*
    init({
        userhash: 'bgdam',
        challenge: 'challenge',
        challengeHash: '4e89d1cbaffb1b4a08aa8a33af5e68ec',
        apiKey: 'api_key'
    });
    */
    global.initAvatarPicker = function(config, _callback) {
        finalCallback = _callback;

        if (!config)
            throw new Error('No config specified');

        addEventListener(window, 'message', function(ev) {
            if (ev.origin == 'https://www.avatarpicker.com' && ev.data) {
                if (ev.data.close)
                    return removeWidget(ev);
                finalAvatarURL = ev.data.avatar;
            }
        });

        init(config);
    };
})(window);
