;
(function() {
    var opts;
    var widgetPrefix;
    var widgetContainer;
    var finalImageURL;
    var username;
    var usernameNode;
    var imageUploadForm;
    var image;
    var sourceFrame;

    var errThreshold = 5;
    var spinnerURL = window.location.protocol + '//www.avatarpicker.com/img/spinner.gif';

    var serviceURLs = {
        avatarpicker: function(username) {
            return window.location.protocol + '//www.avatarpicker.com/avatar/self/' + username + '?s=150';
        },
        gravatar: function(username) {
            return window.location.protocol + '//www.avatarpicker.com/avatar/gravatar/' + username + '?s=150';
        },
        twitter: function(username) {
            return window.location.protocol + '//www.avatarpicker.com/avatar/twitter/' + username + '?s=150';
        },
        facebook: function(username) {
            return window.location.protocol + '//www.avatarpicker.com/avatar/facebook/' + username + '?s=150';
        }
    };

    var supports = {
        fileReader: !!(window.FileReader && window.FileReader.call),
        formData: window.FormData !== undefined
    };
    supports.dragDrop = supports.fileReader && supports.formData;

    var selectedFile;
    var imageContent;

    var defaultOpts = {
        userhash: null,
        challenge: null,
        challengeHash: null,
        apiKey: null,
        select: {
            title: 'Select an Avatar',
            altText: {
                upload: 'Upload an image from your device',
                gravatar: 'Use your Gravatar',
                twitter: 'Use your Twitter picture',
                facebook: 'Use your Facebook profile picture'
            },
            optionTitles: {
                existing: 'Change Existing',
                upload: 'Upload',
                gravatar: 'Gravatar',
                twitter: 'Twitter',
                facebook: 'Facebook'
            },
            buttons: {
                remove: {
                    title: 'Delete Existing Avatar'
                }
            }
        },
        existing: {
            title: 'Use Existing Avatar',
            buttons: {
                back: {
                    title: 'Back'
                },
                ok: {
                    title: 'OK'
                },
                upload: {
                    title: 'Change'
                },
                remove: {
                    title: 'Delete'
                }
            }
        },
        upload: {
            title: 'Upload Avatar',
            buttons: {
                back: {
                    title: 'Back'
                }
            },
            dropCaption: ['', 'Drag and drop a picture', '(or)', 'Click to select a picture'],
            clickCaption: ['', '', 'Click to select a picture']
        },
        edit: {
            title: 'Resize, Crop and Edit',
            buttons: {
                back: {
                    title: 'Back'
                },
                ok: {
                    title: 'OK'
                }
            }
        },
        gravatar: {
            title: 'Use Gravatar Identicon',
            placeholder: 'Ex: bobbytables',
            label: 'Your Gravatar username (or email): ',
            buttons: {
                back: {
                    title: 'Back'
                },
                ok: {
                    title: 'OK'
                }
            }
        },
        twitter: {
            title: 'Use Twitter Profile Picture',
            placeholder: 'Ex: bobbytables',
            label: 'Your Twitter username: ',
            buttons: {
                back: {
                    title: 'Back'
                },
                ok: {
                    title: 'OK'
                }
            }
        },
        facebook: {
            title: 'Use Facebook Profile Picture',
            placeholder: 'Ex: bobbytables',
            label: 'Your Facebook username (or id): ',
            buttons: {
                back: {
                    title: 'Back'
                },
                ok: {
                    title: 'OK'
                }
            }
        },
        'final': {
            title: 'All Done!',
            detail: 'Your avatar has been successfully changed.',
            subDetail: 'Please note that it may take up to three hours for your new avatar to be displayed.',
            buttons: {
                username: {
                    title: 'Change Username'
                },
                ok: {
                    title: 'OK'
                }
            }
        },
        errors: {
            notAnImage: 'Only image files can be uploaded. Please select an image file.',
            imageUploadFailed: 'An error occurred while uploading your image. Please try again.  If the issue persists, please try after a few hours.',
            imageEditFailed: 'An error occurred while trying to edit your avatar image. Please try again. If the issue persists try after a few hours.',
            noImageSelected: 'Please select an image file first.',
            usernameFailed: 'Could not get the {{service}} profile picture for {{username}}. Please re-enter your username.',
            usernameRequired: 'You have to enter your {{service}} username.',
            imageDeleteFailed: 'An error occurred while trying to delete your avatar. Please try again. If the issue persists please try after a few hours.',
        }
    };

    function renderTemplate(template, templateObj) {
        for (var key in templateObj) {
            if (templateObj.hasOwnProperty(key)) {
                var regexp = new RegExp('\\{\\{' + key + '\\}\\}', 'g');
                template = template.replace(regexp, templateObj[key]);
            }
        }

        return template;
    };

    function addEventListener(target, _event, listener) {
        var normalizedListener = function(ev) {
            listener.call(this, normalizeTouchAndMouseEvent(ev));
        };

        var finalListener = listener;
        if (['click', 'mousedown', 'mouseup', 'mousemove'].indexOf(_event) > -1)
            finalListener = normalizedListener;

        if (_event == 'scroll')
            addWheelListener(target, finalListener);
        else if (target.addEventListener)
            target.addEventListener(_event, finalListener);
        else if (target.attachEvent)
            target.attachEvent(_event, finalListener);

        if (_event == 'click' || _event == 'mousedown')
            addEventListener(target, 'touchstart', finalListener);
        else if (_event == 'mousemove')
            addEventListener(target, 'touchmove', finalListener);
        else if (_event == 'mouseup')
            addEventListener(target, 'touchend', finalListener);
    };

    function trimString(str) {
        return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    };

    /**
     * jQuery extend method - standalone version
     * From [https://github.com/dansdom/extend]
     */
    function merge() {
        var options, name, src, copy, copyIsArray, clone,
            target = arguments[0] || {},
            i = 1,
            length = arguments.length,
            deep = false,
            // helper which replicates the jquery internal functions
            objectHelper = {
                hasOwn: Object.prototype.hasOwnProperty,
                class2type: {},
                type: function(obj) {
                    return obj == null ?
                        String(obj) :
                        objectHelper.class2type[Object.prototype.toString.call(obj)] || "object";
                },
                isPlainObject: function(obj) {
                    if (!obj || objectHelper.type(obj) !== "object" || obj.nodeType || objectHelper.isWindow(obj)) {
                        return false;
                    }

                    try {
                        if (obj.constructor &&
                            !objectHelper.hasOwn.call(obj, "constructor") &&
                            !objectHelper.hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                            return false;
                        }
                    } catch (e) {
                        return false;
                    }

                    var key;
                    for (key in obj) {}

                    return key === undefined || objectHelper.hasOwn.call(obj, key);
                },
                isArray: Array.isArray || function(obj) {
                    return objectHelper.type(obj) === "array";
                },
                isFunction: function(obj) {
                    return objectHelper.type(obj) === "function";
                },
                isWindow: function(obj) {
                    return obj != null && obj == obj.window;
                }
            }; // end of objectHelper

        // Handle a deep copy situation
        if (typeof target === "boolean") {
            deep = target;
            target = arguments[1] || {};
            // skip the boolean and the target
            i = 2;
        }

        // Handle case when target is a string or something (possible in deep copy)
        if (typeof target !== "object" && !objectHelper.isFunction(target)) {
            target = {};
        }

        // If no second argument is used then this can extend an object that is using this method
        if (length === i) {
            target = this;
            --i;
        }

        for (; i < length; i++) {
            // Only deal with non-null/undefined values
            if ((options = arguments[i]) != null) {
                // Extend the base object
                for (name in options) {
                    src = target[name];
                    copy = options[name];

                    // Prevent never-ending loop
                    if (target === copy) {
                        continue;
                    }

                    // Recurse if we're merging plain objects or arrays
                    if (deep && copy && (objectHelper.isPlainObject(copy) || (copyIsArray = objectHelper.isArray(copy)))) {
                        if (copyIsArray) {
                            copyIsArray = false;
                            clone = src && objectHelper.isArray(src) ? src : [];

                        } else {
                            clone = src && objectHelper.isPlainObject(src) ? src : {};
                        }

                        // Never move original objects, clone them
                        target[name] = merge(deep, clone, copy);

                        // Don't bring in undefined values
                    } else if (copy !== undefined) {
                        target[name] = copy;
                    }
                }
            }
        }

        // Return the modified object
        return target;
    };

    /**
     * Fast MD5 Implementation
     * http://www.myersdaily.org/joseph/javascript/md5-text.html
     * [From https://gist.githubusercontent.com/PixnBits/8811212/raw/296e873b453f5b125b35554b5ab3bdfe21d5c91e/md5.js]
     */
    (function(global) {

        var md5cycle = function(x, k) {
            var a = x[0],
                b = x[1],
                c = x[2],
                d = x[3];

            a = ff(a, b, c, d, k[0], 7, -680876936);
            d = ff(d, a, b, c, k[1], 12, -389564586);
            c = ff(c, d, a, b, k[2], 17, 606105819);
            b = ff(b, c, d, a, k[3], 22, -1044525330);
            a = ff(a, b, c, d, k[4], 7, -176418897);
            d = ff(d, a, b, c, k[5], 12, 1200080426);
            c = ff(c, d, a, b, k[6], 17, -1473231341);
            b = ff(b, c, d, a, k[7], 22, -45705983);
            a = ff(a, b, c, d, k[8], 7, 1770035416);
            d = ff(d, a, b, c, k[9], 12, -1958414417);
            c = ff(c, d, a, b, k[10], 17, -42063);
            b = ff(b, c, d, a, k[11], 22, -1990404162);
            a = ff(a, b, c, d, k[12], 7, 1804603682);
            d = ff(d, a, b, c, k[13], 12, -40341101);
            c = ff(c, d, a, b, k[14], 17, -1502002290);
            b = ff(b, c, d, a, k[15], 22, 1236535329);

            a = gg(a, b, c, d, k[1], 5, -165796510);
            d = gg(d, a, b, c, k[6], 9, -1069501632);
            c = gg(c, d, a, b, k[11], 14, 643717713);
            b = gg(b, c, d, a, k[0], 20, -373897302);
            a = gg(a, b, c, d, k[5], 5, -701558691);
            d = gg(d, a, b, c, k[10], 9, 38016083);
            c = gg(c, d, a, b, k[15], 14, -660478335);
            b = gg(b, c, d, a, k[4], 20, -405537848);
            a = gg(a, b, c, d, k[9], 5, 568446438);
            d = gg(d, a, b, c, k[14], 9, -1019803690);
            c = gg(c, d, a, b, k[3], 14, -187363961);
            b = gg(b, c, d, a, k[8], 20, 1163531501);
            a = gg(a, b, c, d, k[13], 5, -1444681467);
            d = gg(d, a, b, c, k[2], 9, -51403784);
            c = gg(c, d, a, b, k[7], 14, 1735328473);
            b = gg(b, c, d, a, k[12], 20, -1926607734);

            a = hh(a, b, c, d, k[5], 4, -378558);
            d = hh(d, a, b, c, k[8], 11, -2022574463);
            c = hh(c, d, a, b, k[11], 16, 1839030562);
            b = hh(b, c, d, a, k[14], 23, -35309556);
            a = hh(a, b, c, d, k[1], 4, -1530992060);
            d = hh(d, a, b, c, k[4], 11, 1272893353);
            c = hh(c, d, a, b, k[7], 16, -155497632);
            b = hh(b, c, d, a, k[10], 23, -1094730640);
            a = hh(a, b, c, d, k[13], 4, 681279174);
            d = hh(d, a, b, c, k[0], 11, -358537222);
            c = hh(c, d, a, b, k[3], 16, -722521979);
            b = hh(b, c, d, a, k[6], 23, 76029189);
            a = hh(a, b, c, d, k[9], 4, -640364487);
            d = hh(d, a, b, c, k[12], 11, -421815835);
            c = hh(c, d, a, b, k[15], 16, 530742520);
            b = hh(b, c, d, a, k[2], 23, -995338651);

            a = ii(a, b, c, d, k[0], 6, -198630844);
            d = ii(d, a, b, c, k[7], 10, 1126891415);
            c = ii(c, d, a, b, k[14], 15, -1416354905);
            b = ii(b, c, d, a, k[5], 21, -57434055);
            a = ii(a, b, c, d, k[12], 6, 1700485571);
            d = ii(d, a, b, c, k[3], 10, -1894986606);
            c = ii(c, d, a, b, k[10], 15, -1051523);
            b = ii(b, c, d, a, k[1], 21, -2054922799);
            a = ii(a, b, c, d, k[8], 6, 1873313359);
            d = ii(d, a, b, c, k[15], 10, -30611744);
            c = ii(c, d, a, b, k[6], 15, -1560198380);
            b = ii(b, c, d, a, k[13], 21, 1309151649);
            a = ii(a, b, c, d, k[4], 6, -145523070);
            d = ii(d, a, b, c, k[11], 10, -1120210379);
            c = ii(c, d, a, b, k[2], 15, 718787259);
            b = ii(b, c, d, a, k[9], 21, -343485551);

            x[0] = add32(a, x[0]);
            x[1] = add32(b, x[1]);
            x[2] = add32(c, x[2]);
            x[3] = add32(d, x[3]);

        }

        var cmn = function(q, a, b, x, s, t) {
            a = add32(add32(a, q), add32(x, t));
            return add32((a << s) | (a >>> (32 - s)), b);
        }

        var ff = function(a, b, c, d, x, s, t) {
            return cmn((b & c) | ((~b) & d), a, b, x, s, t);
        }

        var gg = function(a, b, c, d, x, s, t) {
            return cmn((b & d) | (c & (~d)), a, b, x, s, t);
        }

        var hh = function(a, b, c, d, x, s, t) {
            return cmn(b ^ c ^ d, a, b, x, s, t);
        }

        var ii = function(a, b, c, d, x, s, t) {
            return cmn(c ^ (b | (~d)), a, b, x, s, t);
        }

        var md51 = function(s) {
            var txt = '',
                n = s.length,
                state = [1732584193, -271733879, -1732584194, 271733878],
                i;
            for (i = 64; i <= s.length; i += 64) {
                md5cycle(state, md5blk(s.substring(i - 64, i)));
            }
            s = s.substring(i - 64);
            var tail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            for (i = 0; i < s.length; i++)
                tail[i >> 2] |= s.charCodeAt(i) << ((i % 4) << 3);
            tail[i >> 2] |= 0x80 << ((i % 4) << 3);
            if (i > 55) {
                md5cycle(state, tail);
                for (i = 0; i < 16; i++) tail[i] = 0;
            }
            tail[14] = n * 8;
            md5cycle(state, tail);
            return state;
        }

        /* there needs to be support for Unicode here,
         * unless we pretend that we can redefine the MD-5
         * algorithm for multi-byte characters (perhaps
         * by adding every four 16-bit characters and
         * shortening the sum to 32 bits). Otherwise
         * I suggest performing MD-5 as if every character
         * was two bytes--e.g., 0040 0025 = @%--but then
         * how will an ordinary MD-5 sum be matched?
         * There is no way to standardize text to something
         * like UTF-8 before transformation; speed cost is
         * utterly prohibitive. The JavaScript standard
         * itself needs to look at this: it should start
         * providing access to strings as preformed UTF-8
         * 8-bit unsigned value arrays.
         */
        var md5blk = function(s) { /* I figured global was faster.   */
            var md5blks = [],
                i; /* Andy King said do it this way. */
            for (i = 0; i < 64; i += 4) {
                md5blks[i >> 2] = s.charCodeAt(i) + (s.charCodeAt(i + 1) << 8) + (s.charCodeAt(i + 2) << 16) + (s.charCodeAt(i + 3) << 24);
            }
            return md5blks;
        }

        var hex_chr = '0123456789abcdef'.split('');

        var rhex = function(n) {
            var s = '',
                j = 0;
            for (; j < 4; j++)
                s += hex_chr[(n >> (j * 8 + 4)) & 0x0F] + hex_chr[(n >> (j * 8)) & 0x0F];
            return s;
        }

        var hex = function(x) {
            for (var i = 0; i < x.length; i++)
                x[i] = rhex(x[i]);
            return x.join('');
        }

        var md5 = global.md5 = function(s) {
            return hex(md51(s));
        }

        /* this function is much faster,
        so if possible we use it. Some IEs
        are the only ones I know of that
        need the idiotic second function,
        generated by an if clause.  */

        var add32 = function(a, b) {
            return (a + b) & 0xFFFFFFFF;
        }

        if (md5('hello') != '5d41402abc4b2a76b9719d911017c592') {
            var add32 = function(x, y) {
                var lsw = (x & 0xFFFF) + (y & 0xFFFF),
                    msw = (x >> 16) + (y >> 16) + (lsw >> 16);
                return (msw << 16) | (lsw & 0xFFFF);
            }
        }

    })(window);

    // creates a global "addWheelListener" method
    // example: addWheelListener( elem, callback );
    // From: [http://snipplr.com/view.php?codeview&id=68762]
    var addWheelListener;
    (function(window, document) {

        var prefix = "",
            _addEventListener, onwheel, support;

        // detect event model
        if (window.addEventListener) {
            _addEventListener = "addEventListener";
        } else {
            _addEventListener = "attachEvent";
            prefix = "on";
        }

        // detect available wheel event
        if (document.onmousewheel !== undefined) {
            // Webkit and IE support at least "mousewheel"
            support = "mousewheel"
        }
        try {
            // Modern browsers support "wheel"
            WheelEvent("wheel");
            support = "wheel";
        } catch (e) {}
        if (!support) {
            // let's assume that remaining browsers are older Firefox
            support = "DOMMouseScroll";
        }
        addWheelListener = function(elem, callback, useCapture) {
            _addWheelListener(elem, support, callback, useCapture);

            // handle MozMousePixelScroll in older Firefox
            if (support == "DOMMouseScroll") {
                _addWheelListener(elem, "MozMousePixelScroll", callback, useCapture);
            }
        };

        function _addWheelListener(elem, eventName, callback, useCapture) {
            elem[_addEventListener](prefix + eventName, support == "wheel" ? callback : function(originalEvent) {
                !originalEvent && (originalEvent = window.event);

                // create a normalized event object
                var event = {
                    // keep a ref to the original event object
                    originalEvent: originalEvent,
                    target: originalEvent.target || originalEvent.srcElement,
                    type: "wheel",
                    deltaMode: originalEvent.type == "MozMousePixelScroll" ? 0 : 1,
                    deltaX: 0,
                    delatZ: 0,
                    preventDefault: function() {
                        originalEvent.preventDefault ?
                            originalEvent.preventDefault() :
                            originalEvent.returnValue = false;
                    }
                };

                // calculate deltaY (and deltaX) according to the event
                if (support == "mousewheel") {
                    event.deltaY = -1 / 40 * originalEvent.wheelDelta;
                    // Webkit also support wheelDeltaX
                    originalEvent.wheelDeltaX && (event.deltaX = -1 / 40 * originalEvent.wheelDeltaX);
                } else {
                    event.deltaY = originalEvent.detail;
                }

                // it's time to fire the callback
                return callback(event);

            }, useCapture || false);
        }

    })(window, document);

    /**
     * Hidden iFrame file upload
     * From [http://hayageek.com/jquery-ajax-form-submit/]
     */
    function getIFrameDoc(frame) {
        var doc = null;
        // IE8 cascading access check
        try {
            if (frame.contentWindow) {
                doc = frame.contentWindow.document;
            }
        } catch (err) {}
        if (doc) { // successful getting content
            return doc;
        }
        try { // simply checking may throw in ie8 under ssl or mismatched protocol
            doc = frame.contentDocument ? frame.contentDocument : frame.document;
        } catch (err) {
            // last attempt
            doc = frame.document;
        }
        return doc;
    };

    /**
     * Normalizes touch and mouse events
     * Right now, basically copies ev.pageX/Y and ev.touches[0].pageX/Y to ev.normalized.pageX/Y
     * Update as required
     */
    function normalizeTouchAndMouseEvent(ev) {
        ev.normalized = {};

        if (ev.touches && ev.touches[0]) {
            ev.normalized.pageX = ev.touches[0].pageX;
            ev.normalized.pageY = ev.touches[0].pageY;
        } else {
            ev.normalized.pageX = ev.pageX;
            ev.normalized.pageY = ev.pageY;
        }

        return ev;
    };

    function buildImage(src, classes, alt, onCreate, onLoad, onError) {
        var img = {
            element: 'img',
            src: src,
            classNames: classes,
            events: []
        };
        if (alt)
            img.alt = alt;

        if (onCreate && onCreate.call)
            img.onCreate = onCreate;
        if (onLoad && onLoad.call) {
            img.events.push({
                name: 'load',
                listener: function(ev) {
                    if (onError && onError.call && (!this.complete || this.naturalWidth < 2))
                        return onError.call(this, ev);
                    onLoad.call(this, ev);
                }
            });
        }
        if (onError && onError.call) {
            img.events.push({
                name: 'error',
                listener: function(ev) {
                    onError.call(this, ev);
                }
            });
        }

        return img;
    }

    function setOpts(customOpts) {
        return merge(true, {}, defaultOpts, customOpts);
    };

    function buildElementFromArray(elementArray) {
        var elements = [];

        for (var j = 0; j < elementArray.length; ++j) {
            var props = elementArray[j];

            var element;

            if (typeof props == 'string') {
                element = document.createTextNode(props);
            } else {
                if (!props.element)
                    throw new Error('element property mandatory for root and non-text child nodes.');

                element = document.createElement(props.element);
                if (props.classNames)
                    element.className = props.classNames;

                if (props.element == 'button')
                    element.className += ' ' + 'button';

                if (props.events && props.events.length) {
                    for (var i = 0; i < props.events.length; ++i)
                        addEventListener(element, props.events[i].name, props.events[i].listener);
                }

                if (props.children && props.children.length) {
                    var childElements = buildElementFromArray(props.children);
                    for (var i = 0; i < childElements.length; ++i)
                        element.appendChild(childElements[i]);
                }

                var onCreate = props.onCreate;

                delete props.element;
                delete props.events;
                delete props.children;
                delete props.classNames;
                delete props.onCreate;

                for (var key in props) {
                    if (props.hasOwnProperty(key)) {
                        element.setAttribute(key, props[key]);
                        if (key == 'alt')
                            element.setAttribute('title', props[key]);
                    }
                }

                if (onCreate && onCreate.call)
                    onCreate(element);
            }

            elements.push(element);
        }

        return elements;
    };

    /**
     * IE 5.5+, Firefox, Opera, Chrome, Safari XHR object
     *
     * @param string url
     * @param object callback
     * @param mixed data
     * @param null x
     * [From https://gist.github.com/Xeoncross/7663273]
     */
    function ajax(url, callback, data, x) {
        try {
            x = new(this.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
            x.open(data ? 'POST' : 'GET', url, 1);
            x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            x.onreadystatechange = function() {
                x.readyState > 3 && callback && callback(x.responseText, x);
            };
            x.send(data)
        } catch (e) {
            window.console && console.log('Avatarpicker: ' + e);
        }
    };

    /**
     * Not really AJAX. Does a hidden iframe POST.
     * Not really AJAX cuz we don't want to user FormData (IE support)
     * And uploading files w/o formdata is hairy
     */
    function ajaxPost(url, data, onLoad, onError, formNode, formData) {
        data = data || {};

        function handleAjaxResponse(response, x) {
            try {
                response = JSON.parse(response);
            } catch (err) {
                response = {
                    status: 500
                };
            }

            if (response.status !== 200 && onError && onError.call)
                return onError(response.err);

            if (x && x.status && x.status !== 200 && onError && onError.call)
                return onError(x.status);

            if (onLoad && onLoad.call)
                onLoad(response.data);
        };

        if (formData) {
            formData = new FormData();
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    formData.append(key, data[key]);
                }
            }
            ajax(url, handleAjaxResponse, formData);
        } else {
            var containerNode;
            var initialFrameLoad = true;

            var hiddenPostFrame = 'hiddenPostFrame';
            var iframe = {
                element: 'iframe',
                id: hiddenPostFrame,
                name: hiddenPostFrame,
                classNames: 'hidden',
                events: [{
                    name: 'load',
                    listener: function(ev) {
                        if (initialFrameLoad)
                            return;

                        var response;
                        var doc = getIFrameDoc(this); //get iframe Document
                        var docRoot = doc.body ? doc.body : doc.documentElement;

                        var response = docRoot.innerText || docRoot.textContent;
                        document.body.removeChild(containerNode);

                        handleAjaxResponse(response);
                    }
                }]
            };

            var form;
            if (formNode) {
                formNode.setAttribute('target', hiddenPostFrame);
            } else {
                form = {
                    element: 'form',
                    action: url,
                    method: 'POST',
                    target: hiddenPostFrame,
                    onCreate: function(form) {
                        formNode = form;
                    },
                    children: []
                };
            }

            // Add auth data
            data.apiKey = opts.apiKey;
            data.challenge = opts.challenge;
            data.user = opts.userhash;
            data.hash = opts.challengeHash;

            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    if (!formNode) {
                        form.children.push({
                            element: 'input',
                            type: 'hidden',
                            name: key,
                            value: data[key]
                        });
                    } else {
                        formNode.appendChild(buildElementFromArray([{
                            element: 'input',
                            type: 'hidden',
                            name: key,
                            value: data[key]
                        }])[0]);
                    }
                }
            };

            var container = {
                element: 'div',
                classNames: 'ajax-container hidden',
                onCreate: function(div) {
                    containerNode = div;
                },
                children: [iframe]
            };
            if (!formNode)
                container.children.push(form);

            document.body.appendChild(buildElementFromArray([container])[0]);
            initialFrameLoad = false;
            formNode.submit();
        }
    };

    function buildAuthenticatableURL(url, set, sopts) {
        url += '&user=' + encodeURIComponent(opts.userhash) + '&apiKey=' + encodeURIComponent(opts.apiKey) + '&hash=' + encodeURIComponent(opts.challengeHash) + '&challenge=' + encodeURIComponent(opts.challenge);
        if (set)
            url += '&set=true';
        if (sopts)
            url += '&sopts=1';
        url += '&cb=' + Math.random();
        return url;
    };

    function removeWidget(ev) {
        preventEvent(ev);
        window.location.href = window.location.href + '#close';
    };

    function clearWidgetContainer() {
        while (widgetContainer.lastChild)
            widgetContainer.removeChild(widgetContainer.lastChild);

        return widgetContainer;
    };

    function showScreen(screenNode) {
        var currentScreen = document.querySelectorAll('div.screen-active')[0];

        screenNode.className += ' absolute-0';
        widgetContainer.appendChild(screenNode);

        if (currentScreen) {
            currentScreen.className = currentScreen.className.replace('fade-in', '');
            currentScreen.className = currentScreen.className.replace('screen-active', '');
            currentScreen.className += ' fade-out';
            window.setTimeout(function() {
                widgetContainer.removeChild(currentScreen);
            }, 350);
        }

        window.setTimeout(function() {
            screenNode.className += ' fade-in screen-active';
            var screenName = screenNode.getAttribute('data-screen');
            var screenType = screenNode.getAttribute('data-screen-type');
            _gaq && _gaq.push && _gaq.push(['_trackEvent', 'ShowScreen', screenName == 'username' && screenType ? screenType : screenName]);
        }, 1);
    };

    function getScreenHeader(titleText) {
        return {
            element: 'div',
            classNames: 'header',
            children: [{
                element: 'span',
                classNames: 'title',
                children: [titleText]
            }]
        };
    };

    function getScreenFooter(buttons) {
        var neverHideStyle = 'display: inline-block !important; opacity: 1 !important; z-index: 9999999 !important; font-size: 13px !important;';
        var footer = {
            element: 'div',
            classNames: 'footer',
            children: [{
                element: 'span',
                classNames: 'powered-by',
                style: neverHideStyle,
                children: [{
                    element: 'span',
                    style: neverHideStyle,
                    children: [{
                        element: 'a',
                        target: '_blank',
                        href: 'https://www.avatarpicker.com',
                        style: neverHideStyle,
                        children: ['AvatarPicker']
                    }]
                }]
            }, {
                element: 'span',
                classNames: 'buttons',
                children: []
            }]
        };

        for (var key in buttons) {
            var button = {
                element: 'button',
                classNames: key,
                children: [buttons[key].title]
            };
            if (buttons[key].events)
                button.events = buttons[key].events
            if (buttons[key].disabled)
                button.disabled = 'disable';

            footer.children[1].children.push(button);
        }

        return footer;
    };

    function buildSelectScreen(selectOpts) {
        var defaultImages = {
            upload: window.location.protocol + '//www.avatarpicker.com/img/upload.png',
            autogen: window.location.protocol + '//www.avatarpicker.com/img/gravatar.png',
            twitter: window.location.protocol + '//www.avatarpicker.com/img/twitter.png',
            facebook: window.location.protocol + '//www.avatarpicker.com/img/facebook.png',
        };
        var usernames = {
            upload: opts.userhash,
            autogen: opts.gravatar.username,
            twitter: opts.twitter.username,
            facebook: opts.facebook.username
        };
        usernames = {
            upload: usernames.upload ? encodeURIComponent(usernames.upload) : null,
            autogen: usernames.autogen ? encodeURIComponent(md5(trimString(usernames.autogen))) : null,
            twitter: usernames.twitter ? encodeURIComponent(usernames.twitter) : null,
            facebook: usernames.facebook ? encodeURIComponent(usernames.facebook) : null,
        };

        var errCount = {};

        function noImageError(ev, defaultSrc) {
            var option = this.getAttribute('data-option');
            if (errCount[option] > errThreshold)
                return;

            ++errCount[option];
            preventEvent(ev);

            this.src = defaultSrc;

            if (this.className.indexOf('default') == -1)
                this.className += ' default';
        };

        var imageOptionNodes = {};
        var uploadTitleNode;

        function buildSelectOption(title, alt, className, src, defaultImage, initNextScreen) {
            errCount[className] = 0;

            var img = buildImage(src, className, alt, function(img) {
                imageOptionNodes[className] = img;
            }, function(ev) {
                if (className == 'upload' && this.className.indexOf('default') == -1)
                    uploadTitleNode.innerText = uploadTitleNode.textContent = selectOpts.optionTitles.existing;
            }, function(ev) {
                noImageError.call(this, ev, defaultImage);
            });
            img['data-option'] = className;

            return {
                element: 'div',
                classNames: 'image-option ' + className,
                children: [img, {
                    element: 'div',
                    children: [title],
                    onCreate: function(div) {
                        if (className == 'upload')
                            uploadTitleNode = div;
                    }
                }],
                events: [{
                    name: 'mouseup',
                    listener: function(ev) {
                        preventEvent(ev);
                        if (className == 'upload' && imageOptionNodes[className].className.indexOf('default') == -1)
                            return initExistingScreen();
                        initNextScreen();
                    }
                }]
            };
        };

        var spinnerNode;
        var contentNode;
        var content = {
            element: 'div',
            classNames: 'content',
            children: [{
                element: 'div',
                classNames: 'select-options',
                onCreate: function(div) {
                    contentNode = div;
                },
                children: [
                    buildSelectOption(selectOpts.optionTitles.upload, selectOpts.altText.upload, 'upload', usernames.upload ? buildAuthenticatableURL(serviceURLs.avatarpicker(usernames.upload), null, true) : defaultImages.upload, defaultImages.upload, initUploadScreen),
                    buildSelectOption(selectOpts.optionTitles.gravatar, selectOpts.altText.gravatar, 'auto-gen', usernames.autogen ? serviceURLs.gravatar(usernames.autogen) : defaultImages.autogen, defaultImages.autogen, function() {
                        initUsernameScreen('gravatar');
                    }), buildSelectOption(selectOpts.optionTitles.twitter, selectOpts.altText.twitter, 'twitter', usernames.twitter ? buildAuthenticatableURL(serviceURLs.twitter(usernames.twitter)) : defaultImages.twitter, defaultImages.twitter, function() {
                        initUsernameScreen('twitter');
                    }), buildSelectOption(selectOpts.optionTitles.facebook, selectOpts.altText.facebook, 'facebook', usernames.facebook ? serviceURLs.facebook(usernames.facebook) : defaultImages.facebook, defaultImages.facebook, function() {
                        initUsernameScreen('facebook');
                    })
                ]
            }, {
                element: 'div',
                classNames: 'spinner-container hidden',
                onCreate: function(div) {
                    spinnerNode = div;
                },
                children: [{
                    element: 'img',
                    src: spinnerURL,
                }]
            }]
        };

        selectOpts.buttons.remove.events = [{
            name: 'mouseup',
            listener: function(ev) {
                removeAvatar(spinnerNode, contentNode, ev);
            }
        }];

        var header = getScreenHeader(selectOpts.title);
        var footer = getScreenFooter(selectOpts.buttons);

        var _screen = [{
            element: 'div',
            classNames: 'screen screen-select',
            'data-screen': 'select',
            children: [header, content, footer]
        }];

        return buildElementFromArray(_screen)[0];
    };

    function initSelectScreen() {
        username = {};
        var selectScreen = buildSelectScreen(opts.select);
        showScreen(selectScreen);
    };

    function preventEvent(ev) {
        if (ev) {
            ev.preventDefault && ev.preventDefault.call && ev.preventDefault();
            ev.stopPropagation && ev.stopPropagation.call && ev.stopPropagation();
        }
        return false;
    };

    function disableButtons(disable) {
        var footerButtons = document.querySelectorAll('div.footer span.buttons button');
        for (var i = 0; i < footerButtons.length; ++i) {
            if (disable)
                footerButtons[i].setAttribute('disabled', 'disable');
            else
                footerButtons[i].removeAttribute('disabled');
        }
    };

    function toggleVisibleDivs(showThisDiv, hideThisDiv) {
        if (showThisDiv.className.indexOf('spinner-container') > -1)
            disableButtons(true);
        else
            disableButtons(false);

        showThisDiv.className = showThisDiv.className.replace(' hidden', '');
        hideThisDiv.className += ' hidden';
    };

    function removeAvatar(spinner, contentNode, ev) {
        preventEvent(ev);
        if (!confirm('Deleting your avatar is permanent and cannot be undone. Do you still want to delete your avatar?'))
            return;

        toggleVisibleDivs(spinner, contentNode);

        ajaxPost(window.location.protocol + '//www.avatarpicker.com/avatar/remove', {
            remove: true
        }, function() {
            sourceFrame.postMessage({
                avatar: null
            }, '*');
            initSelectScreen();
        }, function(err) {
            alert(opts.errors.imageDeleteFailed);
        });
    };

    function buildUploadScreen(uploadOpts) {
        uploadOpts.buttons.back.events = [{
            name: 'mouseup',
            listener: function(ev) {
                preventEvent(ev);
                initSelectScreen();
            }
        }];

        var header = getScreenHeader(uploadOpts.title);
        var footer = getScreenFooter(uploadOpts.buttons);


        function dragOver(ev) {
            if (this.className.indexOf('border-3dc') == -1)
                this.className += ' border-3dc';

            return preventEvent(ev);
        };

        function removeHighlight(ev) {
            this.className = this.className.replace('border-3dc', '');
        };

        var spinner;
        var contentNode;

        function processImage(file) {
            if (!file.type.match(/image.*/))
                return alert(opts.errors.notAnImage);

            //TODO: add file size check here

            selectedFile = file;

            function uploadError(err) {
                console.log('Avatarpicker: ' + err);
                alert(opts.errors.imageUploadFailed);
                selectedFile = null;
                initSelectScreen();
            };

            function uploadOK(data) {
                imageContent = data;
                image = buildElementFromArray([buildImage(imageContent, 'opaque-0', null, null, initEditScreen, uploadError)])[0];
                document.body.appendChild(image);
            }

            toggleVisibleDivs(spinner, contentNode);
            if (supports.dragDrop) {
                ajaxPost(window.location.protocol + '//www.avatarpicker.com/avatar/upload', {
                    avatarFile: file
                }, uploadOK, uploadError, null, true);
            } else {
                ajaxPost(window.location.protocol + '//www.avatarpicker.com/avatar/upload', null, uploadOK, uploadError, imageUploadForm);
            }
        };

        function drop(ev) {
            var ret = preventEvent(ev);

            removeHighlight.call(this, ev);
            processImage(ev.dataTransfer.files[0]);

            return ret;
        };

        var dropEvents = supports.dragDrop ? [{
            name: 'dragenter',
            listener: dragOver
        }, {
            name: 'dragover',
            listener: dragOver
        }, {
            name: 'drop',
            listener: drop
        }, {
            name: 'mouseleave',
            listener: removeHighlight
        }, {
            name: 'dragleave',
            listener: removeHighlight
        }] : [];

        var dropCaption = [];
        var captionList = dropEvents.length ? uploadOpts.dropCaption : uploadOpts.clickCaption;
        for (var i = 0; i < captionList.length; ++i) {
            dropCaption.push(captionList[i]);
            dropCaption.push({
                element: 'br'
            });
        }
        dropCaption.pop();

        var content = {
            element: 'div',
            classNames: 'content',
            children: [{
                element: 'div',
                classNames: 'spinner-container hidden',
                onCreate: function(div) {
                    spinner = div;
                },
                children: [{
                    element: 'img',
                    src: spinnerURL,
                }]
            }, {
                element: 'div',
                classNames: 'drop-area border-3de',
                onCreate: function(div) {
                    contentNode = div;
                },
                events: dropEvents,
                children: [{
                    element: 'div',
                    classNames: 'caption absolute-0',
                    children: dropCaption
                }, {
                    element: 'form',
                    id: 'upload-image',
                    enctype: 'multipart/form-data',
                    action: window.location.protocol + '//www.avatarpicker.com/avatar/upload',
                    method: 'POST',
                    onCreate: function(form) {
                        imageUploadForm = form;
                    },
                    children: [{
                        element: 'input',
                        type: 'file',
                        name: 'avatarFile',
                        classNames: 'absolute-0',
                        accept: 'image/*',
                        events: [{
                            name: 'change',
                            listener: function(ev) {
                                processImage(this.files[0]);
                                return preventEvent(ev);
                            }
                        }]
                    }]
                }]
            }]
        };

        var _screen = [{
            element: 'div',
            classNames: 'screen screen-upload',
            'data-screen': 'upload',
            children: [header, content, footer]
        }];

        return buildElementFromArray(_screen)[0];
    };

    function initUploadScreen() {
        var uploadScreen = buildUploadScreen(opts.upload);
        showScreen(uploadScreen)
    };

    function buildEditScreen(editOpts) {
        var editAreaSize = {
            width: 244,
            height: 244
        };
        var selAreaSize = {
            width: 150,
            height: 150
        };
        selAreaSize.top = (editAreaSize.height - selAreaSize.height) / 2;
        selAreaSize.left = (editAreaSize.width - selAreaSize.width) / 2;

        function calcImageDimensions(image) {
            var dims = {
                width: selAreaSize.width,
                height: selAreaSize.height,
                left: selAreaSize.left,
                top: selAreaSize.top
            };

            if (image.height > image.width) {
                dims.height = selAreaSize.width * image.height / image.width;
                dims.top = (editAreaSize.height - dims.height) / 2;
            } else {
                dims.width = selAreaSize.height * image.width / image.height;
                dims.left = (editAreaSize.width - dims.width) / 2;
            }

            return dims;
        };

        if (!image) {
            image = new Image();
            image.src = imageContent;
        }
        var imageDims = calcImageDimensions(image);

        function zoomImage(zoomPercent) {
            zoomLevel = 1 + (zoomPercent / 100);

            setTransforms(imageNode);

            moveImage(imageNode, selNode, {
                x: 0,
                y: 0
            });
        };

        var spinner;
        var contentNode;
        var touchData = {
            started: false,
            distance: null
        };

        var zoomLevel;
        var effectiveTransforms = {};

        function calculateTouchData(ev, touchData) {
            if (!(ev.touches && ev.touches.length == 2))
                return;

            var delX = ev.touches[0].pageX - ev.touches[1].pageX;
            var delY = ev.touches[0].pageY - ev.touches[1].pageY;

            touchData.distance = Math.sqrt(Math.pow(delX, 2) + Math.pow(delY, 2));

            return touchData;
        };

        function isPinching(ev, oldTouchData, newTouchData) {
            var distanceThreshold = 3;

            if (Math.abs(oldTouchData.distance - newTouchData.distance) > distanceThreshold)
                return true;

            return false;
        };

        var editArea = {
            element: 'div',
            classNames: 'edit-area full-wh border-3de',
            onCreate: function(div) {
                contentNode = div;
            },
            events: [{
                name: 'scroll',
                listener: function(ev) {
                    //Move thumb by deltaY percent
                    var diffY = (ev.deltaY * 5 / 100 * thumbLowest) / 4;
                    zoomImage(moveThumb(diffY));
                }
            }, {
                name: 'touchstart',
                listener: function(ev) {
                    if (!(ev.touches && ev.touches.length == 2))
                        return;

                    touchData.started = true;
                    touchData = calculateTouchData(ev, touchData);
                }
            }, {
                name: 'touchmove',
                listener: function(ev) {
                    if (!(touchData.started && (ev.touches && ev.touches.length == 2)))
                        return;

                    var currentTouchData = {};
                    currentTouchData = calculateTouchData(ev, currentTouchData);

                    if (!isPinching(ev, touchData, currentTouchData))
                        return;

                    var contentWidth = contentNode.clientWidth;
                    var diffDistance = currentTouchData.distance - touchData.distance;

                    var diffY = diffDistance * thumbLowest / contentWidth;
                    zoomImage(moveThumb(-diffY));

                    touchData = currentTouchData;
                    touchData.started = true;
                }
            }, {
                name: 'touchend',
                listener: function(ev) {
                    touchData.started = false;
                }
            }]
        };

        var selNode;
        var imageNode;
        var imageXY;
        var thumbXY;
        var thumbNode;

        function selMouseDown(ev) {
            if (ev.touches && ev.touches.length == 2)
                return;

            preventEvent(ev);
            imageXY = {
                x: ev.normalized.pageX,
                y: ev.normalized.pageY
            };
        };

        function moveImage(imageNode, selNode, diffXY) {
            var imageBounds = getDivBounds(imageNode, true);
            var selBounds = getDivBounds(selNode, true);

            var diff;
            var newXY = {
                x: imageBounds.left + diffXY.x,
                y: imageBounds.top + diffXY.y
            }

            //Bounds check
            if ((newXY.x + imageBounds.width) < selBounds.right)
                newXY.x = selBounds.right - imageBounds.width;
            if (newXY.x > selBounds.left)
                newXY.x = selBounds.left;
            if ((newXY.y + imageBounds.height) < selBounds.bottom)
                newXY.y = selBounds.bottom - imageBounds.height;
            if (newXY.y > selBounds.top)
                newXY.y = selBounds.top;

            diffXY = {
                x: newXY.x - imageBounds.left,
                y: newXY.y - imageBounds.top
            };
            imageNode.style.left = (parseFloat(imageNode.style.left) + diffXY.x) + 'px';
            imageNode.style.top = (parseFloat(imageNode.style.top) + diffXY.y) + 'px';
        };

        function selMouseMove(ev) {
            if (ev.touches && ev.touches.length == 2)
                return;

            preventEvent(ev);

            if (!(imageXY && imageXY.x && imageXY.y))
                return;

            var diffXY = {
                x: ev.normalized.pageX - imageXY.x,
                y: ev.normalized.pageY - imageXY.y
            };
            imageXY = {
                x: ev.normalized.pageX,
                y: ev.normalized.pageY
            };

            moveImage(imageNode, selNode, diffXY);
        };

        function selMouseUp(ev) {
            if (ev.touches && ev.touches.length == 2)
                return;

            preventEvent(ev);
            if (imageXY) {
                if (ev.target.className.indexOf('select-area') > -1 || ev.target.className.indexOf('image-area') > -1) {
                    moveImage(imageNode, selNode, {
                        x: -1,
                        y: -1
                    });
                }
                imageXY = null;
            }
            if (thumbXY) {
                thumbXY = null;
            }
        };

        var selArea = {
            element: 'div',
            classNames: 'select-area border-3dc',
            style: 'top: ' + selAreaSize.top + 'px; left: ' + selAreaSize.left + 'px; width: ' + selAreaSize.width + 'px; height: ' + selAreaSize.height + 'px;',
            onCreate: function(div) {
                selNode = div;
            },
            events: [{
                name: 'mousedown',
                listener: selMouseDown
            }, {
                name: 'mousemove',
                listener: selMouseMove
            }, {
                name: 'mouseup',
                listener: selMouseUp
            }]
        };

        var imageArea = {
            element: 'div',
            classNames: 'image-area',
            style: 'background-image: url(' + imageContent + '); ' + 'top: ' + imageDims.top + 'px; left: ' + imageDims.left + 'px; width: ' + imageDims.width + 'px; height: ' + imageDims.height + 'px; background-size: 100%;',
            onCreate: function(div) {
                imageNode = div;
            },
            events: [{
                name: 'mousedown',
                listener: selMouseDown
            }, {
                name: 'mousemove',
                listener: selMouseMove
            }, {
                name: 'mouseup',
                listener: selMouseUp
            }]
        };

        var thumbStep = 5; //In percent
        var thumbLowest = 148; //In actual px

        function getDivBounds(divNode, boundingRect) {
            var divBounds;
            if (boundingRect) {
                divBounds = divNode.getBoundingClientRect();
                divBounds.width = divBounds.right - divBounds.left;
                divBounds.height = divBounds.bottom - divBounds.top;
            } else {
                divBounds = {
                    top: parseFloat(divNode.style.top),
                    left: parseFloat(divNode.style.left),
                    width: parseFloat(divNode.style.width),
                    height: parseFloat(divNode.style.height)
                };
                divBounds.right = divBounds.left + divBounds.width;
                divBounds.bottom = divBounds.top + divBounds.height;
            }

            return divBounds;
        };

        function moveThumb(thumbDiff) {
            var thumbTop = parseFloat(thumbNode.style.top);
            thumbTop += thumbDiff;

            thumbTop = thumbTop < 0 ? 0 : thumbTop;
            thumbTop = thumbTop > thumbLowest ? thumbLowest : thumbTop;
            thumbNode.style.top = thumbTop + 'px';

            var thumbValue = 100 - (thumbTop * 100 / thumbLowest);
            return thumbValue;
        };

        var zoomControls = {
            element: 'div',
            classNames: 'zoom-controls',
            events: [{
                name: 'mousemove',
                listener: function(ev) {
                    preventEvent(ev);
                    if (!(thumbXY && thumbXY.y))
                        return;

                    var diffXY = {
                        y: ev.normalized.pageY - thumbXY.y
                    };
                    thumbXY = {
                        y: ev.normalized.pageY
                    };

                    zoomImage(moveThumb(diffXY.y));
                }
            }],
            children: [{
                element: 'button',
                classNames: 'zoom-plus button icon-plus',
                events: [{
                    name: 'mouseup',
                    listener: function(ev) {
                        var step = -1 * (thumbStep * thumbLowest / 100);
                        zoomImage(moveThumb(step));
                    }
                }],
            }, {
                element: 'div',
                classNames: 'zoom-slider',
                children: [{
                    element: 'div',
                    classNames: 'zoom-track',
                }, {
                    element: 'div',
                    classNames: 'zoom-thumb shadow-90',
                    style: 'top: ' + thumbLowest + 'px;',
                    onCreate: function(div) {
                        thumbNode = div;
                    },
                    events: [{
                        name: 'mousedown',
                        listener: function(ev) {
                            preventEvent(ev);
                            thumbXY = {
                                y: ev.normalized.pageY
                            };
                        }
                    }]
                }]
            }, {
                element: 'button',
                classNames: 'zoom-minus button icon-minus',
                events: [{
                    name: 'mouseup',
                    listener: function(ev) {
                        var step = (thumbStep * thumbLowest / 100);
                        zoomImage(moveThumb(step));
                    }
                }]
            }]
        };

        function setTransforms(node, transforms) {
            transforms = transforms || {};
            effectiveTransforms = {
                rotate: 0
            };

            var existingTransforms = {}
            if (node.className.indexOf('rotate-90') > -1)
                existingTransforms.rotate = 90;
            else if (node.className.indexOf('rotate-180') > -1)
                existingTransforms.rotate = 180;
            else if (node.className.indexOf('rotate-270') > -1)
                existingTransforms.rotate = 270;
            if (node.className.indexOf('flip-vertical') > -1)
                existingTransforms.flipVertical = true;
            if (node.className.indexOf('flip-horizontal') > -1)
                existingTransforms.flipHorizontal = true;
            if (zoomLevel)
                existingTransforms.zoomLevel = zoomLevel;

            var styleText = [];
            if (transforms.zoomLevel) {
                styleText.push('scale(' + transforms.zoomLevel + ',' + transforms.zoomLevel + ')');
                effectiveTransforms.scale = transforms.zoomLevel;
            } else if (existingTransforms.zoomLevel) {
                styleText.push('scale(' + existingTransforms.zoomLevel + ',' + existingTransforms.zoomLevel + ')');
                effectiveTransforms.scale = existingTransforms.zoomLevel;
            }

            if (transforms.rotate) {
                styleText.push('rotate(' + transforms.rotate + 'deg)');
                effectiveTransforms.rotate = transforms.rotate;
            } else if (existingTransforms.rotate) {
                styleText.push('rotate(' + existingTransforms.rotate + 'deg)');
                effectiveTransforms.rotate = existingTransforms.rotate;
            }

            if (transforms.flipVertical || existingTransforms.flipVertical) {
                styleText.push('scaleY(-1)');
                imageNode.style.filter = 'flipV';
                imageNode.style['-ms-filter'] = 'FlipV';
                effectiveTransforms.scaleY = -1;
            } else {
                delete imageNode.style.filter;
                delete imageNode.style['-ms-filter'];
            }
            if (transforms.flipHorizontal || existingTransforms.flipHorizontal) {
                styleText.push('scaleX(-1)');
                imageNode.style.filter = 'flipH';
                imageNode.style['-ms-filter'] = 'FlipH';
                effectiveTransforms.scaleX = -1;
            } else {
                delete imageNode.style.filter;
                delete imageNode.style['-ms-filter'];
            }

            styleText = styleText.join(' ');

            imageNode.style['-webkit-transform'] = styleText;
            imageNode.style['-moz-transform'] = styleText;
            imageNode.style['-ms-transform'] = styleText;
            imageNode.style['-o-transform'] = styleText;
            imageNode.style['transform'] = styleText;
        };

        var rotateControls = {
            element: 'div',
            classNames: 'rotate-controls',
            children: [{
                element: 'button',
                draggable: 'false',
                alt: 'Rotate Clockwise',
                classNames: 'rotate-clockwise button icon-redo',
                events: [{
                    name: 'mouseup',
                    listener: function(ev) {
                        preventEvent(ev);

                        var newClass;
                        var oldClass;
                        var styleText;
                        if (imageNode.className.indexOf('rotate-90') > -1) {
                            oldClass = 'rotate-90';
                            newClass = 'rotate-180';
                        } else if (imageNode.className.indexOf('rotate-180') > -1) {
                            oldClass = 'rotate-180';
                            newClass = 'rotate-270';
                        } else if (imageNode.className.indexOf('rotate-270') > -1) {
                            oldClass = 'rotate-270';
                            newClass = null;
                        } else {
                            oldClass = null;
                            newClass = 'rotate-90';
                        }

                        if (oldClass)
                            imageNode.className = imageNode.className.replace(oldClass, '');
                        if (newClass)
                            imageNode.className += ' ' + newClass;

                        setTransforms(imageNode);

                        moveImage(imageNode, selNode, {
                            x: 0,
                            y: 0
                        });
                    }
                }]
            }, {
                element: 'button',
                draggable: 'false',
                alt: 'Rotate Anti-clockwise',
                classNames: 'rotate-anti-clockwise button icon-undo',
                events: [{
                    name: 'mouseup',
                    listener: function(ev) {
                        preventEvent(ev);

                        var newClass;
                        var oldClass;
                        if (imageNode.className.indexOf('rotate-90') > -1) {
                            oldClass = 'rotate-90';
                            newClass = null;
                        } else if (imageNode.className.indexOf('rotate-180') > -1) {
                            oldClass = 'rotate-180';
                            newClass = 'rotate-90';
                        } else if (imageNode.className.indexOf('rotate-270') > -1) {
                            oldClass = 'rotate-270';
                            newClass = 'rotate-180';
                        } else {
                            oldClass = null;
                            newClass = 'rotate-270';
                        }

                        if (oldClass)
                            imageNode.className = imageNode.className.replace(oldClass, '');
                        if (newClass)
                            imageNode.className += ' ' + newClass;

                        setTransforms(imageNode);

                        moveImage(imageNode, selNode, {
                            x: 0,
                            y: 0
                        });
                    }
                }]
            }]
        }

        var flipControls = {
            element: 'div',
            classNames: 'flip-controls',
            children: [{
                element: 'button',
                draggable: 'false',
                classNames: 'button icon-flip2',
                alt: 'Flip Horizontally',
                events: [{
                    name: 'mouseup',
                    listener: function(ev) {
                        preventEvent(ev);

                        var newClass;
                        var oldClass;
                        if (imageNode.className.indexOf('flip-horizontal') > -1) {
                            oldClass = 'flip-horizontal';
                            newClass = null;
                        } else {
                            oldClass = null;
                            newClass = 'flip-horizontal';
                        }

                        if (oldClass)
                            imageNode.className = imageNode.className.replace(' ' + oldClass, '');
                        if (newClass)
                            imageNode.className += ' ' + newClass;

                        setTransforms(imageNode);
                    }
                }]
            }, {
                element: 'button',
                classNames: 'button icon-flip',
                draggable: 'false',
                alt: 'Flip Vertically',
                events: [{
                    name: 'mouseup',
                    listener: function(ev) {
                        preventEvent(ev);

                        var newClass;
                        var oldClass;
                        if (imageNode.className.indexOf('flip-vertical') > -1) {
                            oldClass = 'flip-vertical';
                            newClass = null;
                        } else {
                            oldClass = null;
                            newClass = 'flip-vertical';
                        }

                        if (oldClass)
                            imageNode.className = imageNode.className.replace(' ' + oldClass, '');
                        if (newClass)
                            imageNode.className += ' ' + newClass;

                        setTransforms(imageNode);
                    }
                }]
            }]
        }

        editArea.children = [imageArea, selArea, zoomControls, {
            element: 'div',
            classNames: 'right-controls',
            children: [rotateControls, flipControls]
        }];

        var content = {
            element: 'div',
            classNames: 'content',
            children: [{
                    element: 'div',
                    classNames: 'spinner-container hidden',
                    onCreate: function(div) {
                        spinner = div;
                    },
                    children: [{
                        element: 'img',
                        src: spinnerURL,
                    }]
                },
                editArea
            ],
            events: [{
                name: 'mouseup',
                listener: selMouseUp
            }, {
                name: 'mousemove',
                listener: selMouseUp
            }]
        };

        editOpts.buttons.back.events = [{
            name: 'mouseup',
            listener: function(ev) {
                preventEvent(ev);
                initUploadScreen();
            }
        }];

        editOpts.buttons.ok.events = [{
            name: 'mouseup',
            listener: function(ev) {
                preventEvent(ev);
                var selBounds = getDivBounds(selNode, true);
                var imageBounds = getDivBounds(imageNode, true);

                var diffBounds = {
                    left: selBounds.left - imageBounds.left,
                    top: selBounds.top - imageBounds.top,
                    width: selBounds.width,
                    height: selBounds.height
                };

                function drawSelectedImage(bounds, image) {
                    effectiveTransforms.scale = effectiveTransforms.scale || 1;
                    var newDims = {
                        width: Math.floor(image.width * effectiveTransforms.scale),
                        height: Math.floor(image.height * effectiveTransforms.scale)
                    };

                    if (effectiveTransforms.rotate && effectiveTransforms.rotate != 180) {
                        newDims = {
                            width: newDims.height,
                            height: newDims.width
                        };
                    }

                    var scaledBounds = {
                        left: (diffBounds.left * newDims.width / imageBounds.width) / effectiveTransforms.scale,
                        top: (diffBounds.top * newDims.height / imageBounds.height) / effectiveTransforms.scale,
                        width: (diffBounds.width * newDims.width / imageBounds.width) / effectiveTransforms.scale,
                        height: (diffBounds.height * newDims.height / imageBounds.height) / effectiveTransforms.scale
                    };

                    toggleVisibleDivs(spinner, contentNode);
                    ajaxPost(window.location.protocol + '//www.avatarpicker.com/avatar/edit', {
                        boundWidth: scaledBounds.width,
                        boundHeight: scaledBounds.height,
                        boundTop: scaledBounds.top,
                        boundLeft: scaledBounds.left,
                        rotate: effectiveTransforms.rotate,
                        flipH: effectiveTransforms.scaleX == -1 ? -1 : 1,
                        flipV: effectiveTransforms.scaleY == -1 ? -1 : 1
                    }, function(data) {
                        finalImageURL = serviceURLs.avatarpicker(opts.userhash);
                        initFinalScreen();
                    }, function(err) {
                        alert(opts.errors.imageEditFailed);
                        window.setTimeout(function() {
                            initUploadScreen();
                        }, 300);
                    });
                };

                drawSelectedImage(selBounds, image);
            }
        }];

        var header = getScreenHeader(editOpts.title);
        var footer = getScreenFooter(editOpts.buttons);

        var _screen = [{
            element: 'div',
            classNames: 'screen screen-edit',
            'data-screen': 'edit',
            children: [header, content, footer]
        }];

        return buildElementFromArray(_screen)[0];
    };

    function initEditScreen() {
        if (!selectedFile) {
            alert(opts.errors.noImageSelected);
            return initUploadScreen();
        }

        var editScreen = buildEditScreen(opts.edit);
        showScreen(editScreen);
    };

    function buildFinalScreen(finalOpts) {
        var spinner;
        var detailNode;

        function imageError(ev, noAlert) {
            if (username.name && username.service) {
                if (!noAlert)
                    alert(renderTemplate(opts.errors.usernameFailed, {
                        service: username.service,
                        username: username.name
                    }));

                finalImageURL = null;
                opts[username.service].username = null;

                // Have to use settimeout or multiple screens are set as active
                // screwing up back button on username screen
                window.setTimeout(function() {
                    initUsernameScreen(username.service);
                }, 300);
            }
        };

        finalOpts.buttons.username.events = [{
            name: 'mouseup',
            listener: function(ev) {
                imageError(ev, true);
            }
        }];
        finalOpts.buttons.ok.events = [{
            name: 'mouseup',
            listener: function(ev) {
                sourceFrame.postMessage({
                    close: true
                }, '*');
            }
        }];

        finalOpts.buttons.ok.disabled = true;
        finalOpts.buttons.username.disabled = true;

        if (!username.service)
            delete finalOpts.buttons.username;

        var header = getScreenHeader(finalOpts.title);
        var footer = getScreenFooter(finalOpts.buttons);

        var content = {
            element: 'div',
            classNames: 'content',
            children: [{
                element: 'div',
                classNames: 'spinner-container',
                onCreate: function(div) {
                    spinner = div;
                },
                children: [{
                    element: 'img',
                    src: spinnerURL,
                }]
            }, {
                element: 'div',
                classNames: 'detail-text hidden',
                onCreate: function(div) {
                    detailNode = div;
                },
                children: [buildImage(finalImageURL, 'image-final', null, null, function(ev) {
                        toggleVisibleDivs(detailNode, spinner);
                        sourceFrame.postMessage({
                            avatar: finalImageURL
                        }, '*');
                    }, imageError),
                    finalOpts.detail, {
                        element: 'br'
                    }, {
                        element: 'small',
                        children: [finalOpts.subDetail]
                    }
                ]
            }]
        };

        var _screen = [{
            element: 'div',
            classNames: 'screen screen-final',
            'data-screen': 'final',
            children: [header, content, footer]
        }];

        return buildElementFromArray(_screen)[0];
    };

    function initFinalScreen() {
        if (!finalImageURL && username.name) {
            username.finalName = encodeURIComponent(username.finalName);

            if (serviceURLs[username.service] && serviceURLs[username.service].call)
                finalImageURL = serviceURLs[username.service](username.finalName);
            else
                finalImageURL = '';

        }

        //bust caches && authenticate
        //finalImageURL = buildAuthenticatableURL(finalImageURL, true);

        ajaxPost(window.location.protocol + '//www.avatarpicker.com/avatar/set/', {
            service: username.service || 'self',
            username: username.finalName || opts.userhash
        }, function() {
            if (!username.service)
                finalImageURL = buildAuthenticatableURL(finalImageURL);

            var finalScreen = buildFinalScreen(opts['final']);
            showScreen(finalScreen);
        }, function(err) {
            if (username.name && username.service) {
                alert(renderTemplate(opts.errors.usernameFailed, {
                    service: username.service,
                    username: username.name
                }));

                finalImageURL = null;
                opts[username.service].username = null;
                initUsernameScreen(username.service);
            }
        });
    };

    function getServiceDisplayName(service) {
        var serviceDisplayName = service == 'twitter' ? 'Twitter' : '';
        serviceDisplayName = service == 'facebook' ? 'Facebook' : serviceDisplayName;
        serviceDisplayName = service == 'gravatar' ? 'Gravatar' : serviceDisplayName;
        return serviceDisplayName;
    };

    function triggerUsernameOK(service) {
        username = {
            name: trimString(usernameNode.value),
            service: service
        };

        if (!username.name.length)
            return alert(renderTemplate(opts.errors.usernameRequired, {
                service: getServiceDisplayName(username.service)
            }));

        username.finalName = service == 'gravatar' ? md5(username.name) : username.name;

        initFinalScreen();
    };

    function buildUsernameScreen(usernameOpts, screenName) {
        usernameOpts.buttons.back.events = [{
            name: 'mouseup',
            listener: function(ev) {
                preventEvent(ev);
                initSelectScreen();
            }
        }];

        usernameOpts.buttons.ok.events = [{
            name: 'mouseup',
            listener: function(ev) {
                triggerUsernameOK(screenName);
            }
        }];

        var header = getScreenHeader(usernameOpts.title);
        var footer = getScreenFooter(usernameOpts.buttons);

        var content = {
            element: 'div',
            classNames: 'content',
            children: [{
                element: 'div',
                children: [{
                    element: 'label',
                    'for': 'in-username',
                    children: [usernameOpts.label]
                }, {
                    element: 'input',
                    type: 'text',
                    id: 'in-username',
                    placeholder: usernameOpts.placeholder,
                    events: [{
                        name: 'keyup',
                        listener: function(ev) {
                            var code = ev.keyCode || ev.which;
                            if (code != 13)
                                return;
                            triggerUsernameOK(screenName);
                        }
                    }],
                    onCreate: function(input) {
                        usernameNode = input;
                    }
                }]
            }]
        };

        var _screen = [{
            element: 'div',
            classNames: 'screen screen-username',
            'data-screen': 'username',
            'data-screen-type': screenName,
            children: [header, content, footer]
        }];

        return buildElementFromArray(_screen)[0];
    };

    function initUsernameScreen(screenName) {
        screenName = screenName || 'facebook';

        var usernameOpts = opts[screenName];
        var finalUsername = usernameOpts.username;
        if (screenName == 'gravatar' && finalUsername)
            finalUsername = md5(trimString(finalUsername));

        if (finalUsername) {
            username = {
                name: usernameOpts.username,
                finalName: finalUsername,
                service: screenName
            };
            return initFinalScreen();
        }

        var usernameScreen = buildUsernameScreen(usernameOpts, screenName);
        showScreen(usernameScreen);

        usernameNode.focus();
    };

    function buildExistingScreen(existingOpts) {
        existingOpts.buttons.back.events = [{
            name: 'mouseup',
            listener: function(ev) {
                preventEvent(ev);
                initSelectScreen();
            }
        }];

        existingOpts.buttons.ok.events = [{
            name: 'mouseup',
            listener: function(ev) {
                finalImageURL = serviceURLs.avatarpicker(opts.userhash);
                initFinalScreen();
            }
        }];

        var header = getScreenHeader(existingOpts.title);
        var footer = getScreenFooter([existingOpts.buttons.back, existingOpts.buttons.ok]);

        var spinner;
        var contentNode;
        var deleteForm;
        var content = {
            element: 'div',
            classNames: 'content',
            children: [{
                element: 'div',
                classNames: 'spinner-container hidden',
                onCreate: function(div) {
                    spinner = div;
                },
                children: [{
                    element: 'img',
                    src: spinnerURL,
                }]
            }, {
                element: 'div',
                classNames: 'existing-area overflow-ok',
                onCreate: function(div) {
                    contentNode = div;
                },
                children: [{
                    element: 'img',
                    classNames: 'image-existing',
                    src: buildAuthenticatableURL(serviceURLs.avatarpicker(opts.userhash), null, true),
                }, {
                    element: 'div',
                    classNames: 'existing-buttons overflow-ok',
                    children: [{
                        element: 'button',
                        children: [existingOpts.buttons.upload.title],
                        events: [{
                            name: 'mouseup',
                            listener: function(ev) {
                                preventEvent(ev);
                                initUploadScreen();
                            }
                        }]
                    }, {
                        element: 'button',
                        children: [existingOpts.buttons.remove.title],
                        events: [{
                            name: 'mouseup',
                            listener: function(ev) {
                                removeAvatar(spinner, contentNode, ev);
                            }
                        }]
                    }]
                }]
            }]
        };

        var _screen = [{
            element: 'div',
            classNames: 'screen screen-existing',
            'data-screen': 'existing',
            children: [header, content, footer]
        }];

        return buildElementFromArray(_screen)[0];
    };

    function initExistingScreen() {
        var existingScreen = buildExistingScreen(opts.existing);
        showScreen(existingScreen);
    };

    function wrapOpt(queryOpts, services) {
        for (var i = 0; i < services.length; ++i) {
            var service = services[i];
            if (queryOpts[service]) {
                queryOpts[service] = {
                    username: queryOpts[service]
                }
            }
        }
        return queryOpts;
    };

    function getQueryOpts() {
        var queryParams = window.location.search.substr(1);
        if (!queryParams)
            return null;

        var queryOpts = {};
        queryParams = queryParams.split('&');
        for (var i = 0; i < queryParams.length; ++i) {
            var param = queryParams[i].split('=');
            if (param[0])
                queryOpts[param[0]] = param[1];
        }
        queryOpts = wrapOpt(queryOpts, ['gravatar', 'twitter', 'facebook']);
        return queryOpts;
    };

    function init(customOpts) {
        //var customOpts = getQueryOpts();
        opts = setOpts(customOpts);

        widgetPrefix = '_' + Math.random().toString().substr(2);

        if (!opts.userhash)
            throw new Error('No userhash provided. Userhash is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/integration#userhash-example for details.');
        if (!opts.challenge)
            throw new Error('No challenge provided. Challenge is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/integration#challenge-example for details.');
        if (!opts.challengeHash)
            throw new Error('No challengeHash provided. Challenge hash is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/https://www.avatarpicker.com/docs/integration#challengehash-example for details.');
        if (!opts.apiKey)
            throw new Error('No apiKey provided. API key is a mandatory parameter. Refer docs at https://www.avatarpicker.com/docs/https://www.avatarpicker.com/docs/integration#apikey-example for details.');

        if (opts.customCSS) {
            var customStyleNode = document.querySelectorAll('style#custom-css')[0];
            if (customStyleNode) {
                customStyleNode.innerText = opts.customCSS;
            }
        }

        widgetContainer = document.body;
        initSelectScreen();
    };

    //Testing only
    /*
    supports = {
        fileReader: false,
        canvas: false
    };
    */

    addEventListener(window, 'message', function(ev) {
        sourceFrame = ev.source;
        init(JSON.parse(ev.data));
    });
})();
